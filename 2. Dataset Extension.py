"""
This script uses the K-fold models to train a train a model that makes use of an extended dataset (pseudo-labelling)
"""
#%% Setup
from pickle import dump, load
import numpy  as np
import pandas as pd

import torch

import classification_transforms as T
import Dataset                   as ds
import MobileNetV2
import CustomNet
import CustomNetV2
import Loss
import Utils
import Logger

from PosteriorAdjustment import posterior_adjustment

from torchvision      import transforms
from torchtools.optim import RangerLars

torch.cuda.is_available()

#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.ChooseOne([ #T.RandomNoise(decay=1/600e3, decay_stop=0.8, probability=0.5)
                                                     T.RandomErasures(n_erasures=[1,3], h=[0.15, 0.4], w=[0.15, 0.4],  decay=1/600e3, decay_stop=0.9, probability=1.0)
                                                   , T.RandomAffine(decay=1/600e3, decay_stop=0.5, probabilities=[0.7,0.7,0.7,0.7,0.7,0.7])
                                                   , T.RandomCrop(decay=1/600e3, decay_stop=0.3, probability=0.5)
                                                   #, T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.2,0.2,0.2,0.2,0.2])
                                                   , T.Skew(probability=1.0)
                                                   , T.RandomDistortion(probability=1.0, grid=[5,5], magnitude=9)
                                                   ]
                                                  )
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([transforms.ToTensor()])

# Load the data
df_1 = pd.read_csv("dataset/train labels.csv")
df_2 = pd.read_csv("dataset/Dig-MNIST labels.csv")
df_3 = pd.read_csv("dataset/test labels.csv")

# Create the datasets
ds_1 = ds.KannadaMNIST( image_folder="dataset/train"
                      , labels      =df_1
                      , transforms  =test_transform
                      )

ds_2 = ds.KannadaMNIST( image_folder="dataset/Dig-MNIST"
                      , labels      =df_2
                      , transforms  =test_transform
                      )

ds_3 = ds.KannadaMNIST( image_folder="dataset/test"
                      , labels      =df_3
                      , transforms  =test_transform
                      )

# Create loaders
loader_1 = torch.utils.data.DataLoader(dataset=ds_1, batch_size=128, shuffle=False)
loader_2 = torch.utils.data.DataLoader(dataset=ds_2, batch_size=128, shuffle=False)
loader_3 = torch.utils.data.DataLoader(dataset=ds_3, batch_size=128, shuffle=False)

#%% Model ensembles
if False:
    # Prediction loss
    predict_loss = Loss.CrossEntropyConfidenceLoss(reduce=False)

    # Load the K-fold models
    models      = []
    predictions = {"train":[], "Dig-MNIST":[], "test":[]}
    for i in range(5):
        # Instantiate the model
        model = CustomNetV2.KannadaNet(input_shape=[1, 28, 28], n_class=10)

        # Load the weights
        model.load_state_dict(torch.load("checkpoints/CustomNetV2-{} Best Weights.tar".format(i))["model_state_dict"])

        # Activate evaluation
        model.eval()
        model.cuda()
        models.append(model)

    # Predictions for each model and loader
    for name, loader in zip(["train", "Dig-MNIST", "test"], [loader_1, loader_2, loader_3]):
        with torch.no_grad():
            predictions[name] = [Utils.predict( loader =loader
                                              , net    =model
                                              , loss_fn=predict_loss if name is not "test" else None
                                              ) for model in models
                                ]

    # Save results
    with open("dataset\\k-fold_predictions.pth", 'wb') as f:
        dump(predictions, f)

#%% Load results
with open("dataset\\k-fold_predictions.pth", 'rb') as f:
    predictions = load(f)

#%% Dataset modification
# Aggregate the metrics
# Training set
losses = torch.stack([result["losses"] for result in predictions["train"]], dim=1).mean(dim=1)
ids    = predictions["train"][0]["ids"]

ds_1.drop_extrema(ids=ids, losses=losses, p_easy=0.001, p_hard=0.01)
ds_1.transforms = train_transform


# Dig-MNIST set
predictions_ = torch.argmax(torch.stack([result["predictions"] for result in predictions["Dig-MNIST"]], dim=1).mean(dim=1), dim=1)
labels       = predictions["Dig-MNIST"][0]["labels"]
losses       = torch.stack([result["losses"] for result in predictions["Dig-MNIST"]], dim=1).mean(dim=1)
ids          = predictions["Dig-MNIST"][0]["ids"]

dig_labels = Utils.extend_dataset( results={ "predictions": predictions_
                                           , "labels"     : labels
                                           , "losses"     : losses
                                           , "ids"        : ids
                                           }
                                 , p_easy =0.001
                                 , p_hard =0.01
                                 )
dig_ds = ds.KannadaMNIST( image_folder="dataset/Dig-MNIST"
                        , labels      =dig_labels
                        , transforms  =train_transform
                        )

# Test set
predictions_ = torch.argmax(torch.stack([result["predictions"] for result in predictions["test"]], dim=1).mean(dim=1), dim=1)
confidences  = torch.stack([result["confidences"] for result in predictions["test"]], dim=1).mean(dim=1)
ids          = predictions["test"][0]["ids"]

pseudo_labels = Utils.pseudo_label( results ={ "predictions": predictions_
                                             , "confidences": confidences
                                             , "ids"        : ids
                                             }
                                  , label_df=df_3
                                  )
pseudo_ds = ds.KannadaMNIST( image_folder="dataset/test"
                           , labels      =pseudo_labels
                           , transforms  =train_transform
                           )

# Create a new training loader
train_loader = torch.utils.data.DataLoader( dataset   =torch.utils.data.ConcatDataset([ds_1, dig_ds, pseudo_ds])
                                          , batch_size=128
                                          , shuffle   =True
                                          )

#%% Network
net = CustomNetV2.KannadaNet( input_shape=[1, 28, 28]
                            , n_class    =10
                            )

net.cuda()

# Loss
loss_fn = Loss.CrossEntropyConfidenceLoss()

#%% Training
# Optimizer
#optimizer = RangerLars(params=net.parameters())
optimizer = torch.optim.RMSprop(params=net.parameters(), lr=0.0001)

# scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=0.0001, steps_per_epoch=len(train_loader), epochs=400)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( optimizer=optimizer
                                                      , mode     ="min"
                                                      , factor   =0.5
                                                      , patience =4
                                                      , verbose  =1
                                                      , min_lr   =1e-9
                                                      , cooldown =0
                                                      )

# Train for 100 epochs
best_acc, epoch = Utils.train( name          ="CustomNetV2-Extended"
                             , train_loader  =train_loader
                             , test_loaders  =[loader_2, loader_1]
                             , net           =net
                             , loss_fn       =loss_fn
                             , optimizer     =optimizer
                             , scheduler     =scheduler
                             , scheduler_type="per epoch"
                             , logger        =Logger.Logger
                             , epoch_start   =1
                             , epoch_end     =100
                             , early_stopping=20
                             , half          =False
                             )
