"""
Main script for handling all training
"""
#%% Setup
import torch

import transform      as ts
from deprecated import model as md

torch.cuda.is_available()

#%% Transforms
# Transforms
# Train transform
train_transform = ts.Transform( transforms=[ ts.Expand(filler=(1, 1, 1), max_scale=2, chance=0.5)
                                           , ts.Grayscale()
                                           #, ts.Flip(chance=0.5)
                                           , ts.Resize(dims=(128,128))
                                           , ts.PhotometricDistort(chance=0.5)

                                           ]
                              , mean=[0.0692]
                              , std =[0.2051]
                              )

# Test transform
test_transform = ts.Transform( transforms=[ ts.Resize(dims=(128,128))
                                          , ts.Grayscale()
                                          ]
                             , mean=[0.0692]
                             , std =[0.2051]
                             )

transforms = {"train":train_transform, "test":test_transform}

#%% Dataset setup
# Define the classes for each feature
feature_classes = { #"consonant_diacritic":[[0], [2,4,5], [1,3,6]]
                  #, "vowel_diacritic"    :[[0, 1], [5,6,8,10], [2,3,4,7,9]]
                    "grapheme_root"      :[ [72,64,13,107,23,96,113,147,133,115,53,43,103,79,81,38,159,22,71]
                                          , [29,56,149,59,89,122,139,150,124,86,123,42,76,141,148,55,18,167,153,91,119,117,74,83,48]
                                          , [65,85,58,151,120,165,118,25,75,32,92,15,101,142,44,136,132,36,129,94,70,77,128,21,16,52,138,155,127,109,112,140,62,125,14,156,111]
                                          , [66,31,17,88,28,40,69,106,50,154,95,135,57,68,98,90,134,93,39,144,143,152,54,46,61,137,97,160]
                                          , [35,67,162,121,84,99,80,47,100,9,131,30,116,60,110,2,24,20,4,51,157,49,3,145,161,6,166,27,41,146,78,82]
                                          , [37,19,34,5,26,87,163,164,104,126,108,8,7,10,105,11,114,63,0,12,1,45,130,158,102,33,73]
                                          ]
                  }

#%% Model Weights
weights = { "consonant_diacritic": "checkpoints\\MobileNetV2 consonant-vowel weights.tar"
          , "vowel_diacritic"    : "checkpoints\\MobileNetV2 consonant-vowel weights.tar"
          , "grapheme_root"      : "checkpoints\\MobileNetV2 grapheme_root weights.tar"
          }

#%% Testing
'''
model = md.GraphemeModel( feature     ="consonant_diacritic"
                        , classes     =feature_classes["consonant_diacritic"][1]
                        , classes_name=1
                        , transforms  =transforms
                        , weights     ="checkpoints\\MobileNetV2 grapheme weights.tar"
                        )

model.train(epochs=20, save_every=1)
'''

#%% Training
# Create and train each model
for feature, classes in feature_classes.items():
    for i,classes_ in enumerate(classes):
        print("Feature: {} Classes: {}".format(feature, i))
        model = md.GraphemeModel( feature     =feature
                                , classes     =classes_
                                , classes_name=i
                                , transforms  =transforms
                                , weights     =weights[feature]
                                )

        model.train(epochs=40, save_every=1)

        model.save(root_folder="checkpoints\\models")

        # Ensure that memory has been freed
        del model
