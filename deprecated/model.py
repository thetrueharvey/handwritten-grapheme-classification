"""
Class for controlling models
"""
#%% Setup
from pickle import dump, load
from tqdm   import tqdm

import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

from deprecated import dataset_subset as ds

from torchtools.optim import RangerLars
from models import MobileNetV2
from Loss   import CrossEntropyHardNegative

#%% Model class
class GraphemeModel:
    def __init__( self
                , feature     =None
                , classes     =None
                , classes_name=None
                , transforms  =None
                , net         =None
                , weights     =None
                , loss_fn     =None
                , optimizer   =None
                , batch_size  =128
                , model_id    =None
                ):
        # Class attributes
        self.classes = classes

        # Create a dataset
        self.dataset = ds.GraphemeDataSubset( image_folder    ="dataset\\images"
                                            , label_file      ="../dataset/bengaliai-cv19/train.csv"
                                            , feature         =feature
                                            , classes         =classes
                                            , train_transforms=transforms["train"]
                                            , test_transforms =transforms["test"]
                                            , include_negative=True
                                            )

        # Create a loader
        self.loader = torch.utils.data.DataLoader( dataset   =self.dataset
                                                 , batch_size=batch_size
                                                 , shuffle   =True
                                                 # , num_workers=4
                                                 , pin_memory=True
                                                 )

        # Class attributes
        self.net = MobileNetV2.MobileNetV2( n_class    =len(classes) + 1
                                          , input_shape=[1,128,128]
                                          , name       ="MobileNetV2 {} {}".format(feature, classes if classes_name is None else classes_name)
                                          )

        # Load the pretrained weights
        self.load_weights(weights)

        # Push the model to GPU
        self.net.cuda()

        self.loss_fn  =CrossEntropyHardNegative(ratio=0.75) if loss_fn is None else loss_fn
        self.optimizer=RangerLars(self.net.parameters()) if optimizer is None else optimizer
        self.model_id =self.net.name if model_id is None else model_id

        # Initialize DataFrame storage
        self.train_df = pd.DataFrame()
        self.test_df  = pd.DataFrame()

    def _train_step(self, epoch, print_every=100):
        """
        Handles a single training step
        :return: None
        """
        # Storage
        train_predictions = []
        train_truths      = []
        train_loss        = []

        # Training
        self.dataset.train()
        self.net.train()
        for i, sample in enumerate(self.loader):
            self.optimizer.zero_grad()

            # Forward pass
            out = self.net(sample["images"].cuda())

            # Predictions
            predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

            # Loss
            loss_ = self.loss_fn(out.cpu(), sample["classes"])

            # Backward propogation
            loss_.backward()
            self.optimizer.step()

            # Metrics
            train_predictions.append(predictions)
            train_truths.append(sample["classes"].numpy())
            train_loss.append(loss_.detach().item())

            # Print a summary every minibatches
            if (i + 1) % print_every == 0:
                predictions = np.concatenate(train_predictions[-print_every:])
                truths      = np.concatenate(train_truths[-print_every:])
                loss        = np.array(train_loss[-print_every:]).mean()

                accuracy = np.equal(predictions, truths).astype(np.double).mean()

                print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

        # Create a temporary DataFrame
        temp_train_df = pd.DataFrame({ "Epoch"     : epoch
                                     , "Prediction": np.concatenate(train_predictions)
                                     , "Target"    : np.concatenate(train_truths)
                                     }
                                    )

        return temp_train_df

    def _eval_step(self, epoch):
        # Storage
        test_predictions = []
        test_truths      = []
        test_loss        = []

        # Testing
        self.dataset.eval()
        self.net.eval()
        with torch.no_grad():
            for i, sample in enumerate(self.loader):
                # Forward pass
                out = self.net(sample["images"].cuda())

                # Predictions
                predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

                # Loss
                loss_ = self.loss_fn(out.cpu(), sample["classes"])

                # Metrics
                test_predictions.append(predictions)
                test_truths.append(sample["classes"].numpy())
                test_loss.append(loss_.detach().item())

        # Print a summary of evaluation
        predictions = np.concatenate(test_predictions)
        truths      = np.concatenate(test_truths)
        loss        = np.array(test_loss).mean()

        accuracy = np.equal(predictions, truths).astype(np.double).mean()

        print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

        # Create a temporary DataFrame
        temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                    , "Prediction": np.concatenate(test_predictions)
                                    , "Target"    : np.concatenate(test_truths)
                                    }
                                   )

        return accuracy, temp_test_df

    def train(self, epochs, eval_every=1, print_every=100, save_every=5, early_stop=3):
        """
        Trains a model for the desired number of epochs
        :param epochs:
        :param eval_every:
        :param print_every:
        :param save_every:
        :return:
        """
        # Initialize accuracy and overfit steps
        test_accuracy = 0

        # Training loop
        for epoch in range(1, epochs + 1):
            # Training
            temp_train_df = self._train_step(epoch=epoch, print_every=print_every)

            # Update the training table
            self.train_df = pd.concat([self.train_df, temp_train_df])

            # Training summary
            train_df_summary = temp_train_df.eval("Correct = Target == Prediction").groupby("Target")["Correct"].mean().reset_index()

            # Print a full summary every epoch
            print("Epoch {} training summary:".format(epoch))
            print(train_df_summary)

            # Evaluation
            if eval_every is not None and epoch % eval_every == 0:
                accuracy, temp_test_df = self._eval_step(epoch=epoch)

                # Update the testing table
                self.test_df = pd.concat([self.test_df, temp_test_df])

                # Testing summary
                test_df_summary = temp_test_df.eval("Correct = Target == Prediction").groupby("Target")["Correct"].mean().reset_index()

                print("Epoch {} testing summary:".format(epoch))
                print(test_df_summary)

                # Check accuracy
                if accuracy > test_accuracy:
                    test_accuracy = accuracy
                    overfit_steps = 0
                else:
                    overfit_steps = overfit_steps + 1

            # Early stopping
            if overfit_steps == early_stop:
                print("Stopping training as testing loss is diverging")
                break


            # Save a distinct checkpoint when requested
            if epoch % save_every == 0:
                torch.save({ "epoch"               : epoch
                           , "model_state_dict"    : self.net.state_dict()
                           , "optimizer_state_dict": self.optimizer.state_dict()
                           , "train_df"            : self.train_df
                           , "test_df"             : self.test_df
                           }
                          , "checkpoints\\{} - checkpoint {}.tar".format(self.model_id, epoch)
                          )

    def load_weights(self, weights):
        """
        Loads weights into the model. Used for warm-starting models
        :param state_dict:
        :return:
        """
        if weights is not None:
            state_dicts = torch.load(weights)
            model_state_dict = state_dicts["model_state_dict"]

            # Remove fully connected layers
            del model_state_dict["layers.19.weight"]
            del model_state_dict["layers.19.bias"]

            self.net.load_state_dict(model_state_dict, strict=False)

    def save(self, root_folder):
            """
            Saves the model class in its current state
            :param path:
            :return:
            """
            with open(root_folder + "\\" + self.model_id, 'wb') as f:
                dump(self, f)

    @classmethod
    def load(cls, path):
        """
        Restores this class to the saved version
        :param path:
        :return:
        """
        with open(path, 'rb') as f:
            return load(f)

    def predict(self, loader):
        """
        Provides predictions for the provided samples in the loader, mapping back to the original feature space
        :param samples:
        :return:
        """
        # Ensure that the model is on GPU
        self.net.cuda()

        # Initialize storage
        results = []

        self.net.eval()
        with torch.no_grad():
            for sample in tqdm(loader):
                # Return the logit predictions
                out = self.net(sample["images"].cuda())
                
                # Softmax
                out = F.softmax(out).cpu().detach()

                # Save to storage
                results.append(out.numpy())

        # Concatenate the results and remove the dummy column
        results = np.concatenate(results)[:,1:]

        return results

