"""
Script for testing and investigating the dataset
"""
#%% Setup
# Installs
# conda install pyarrow -c conda-forge

# Libraries
import numpy   as np
import pandas  as pd

import PIL.Image as Image, PIL.ImageDraw as ImageDraw, PIL.ImageFont as ImageFont

import matplotlib.pyplot as plt

#%% Constants
HEIGHT = 137
WIDTH  = 236

#%% Functions
def load_as_npa(file):
    df = pd.read_parquet(file)
    return df.iloc[:, 0], df.iloc[:, 1:].values.reshape(-1, HEIGHT, WIDTH)

def image_from_char(char):
    image = Image.new('RGB', (WIDTH, HEIGHT))
    draw = ImageDraw.Draw(image)
    myfont = ImageFont.load("arial.pil")
    w, h = draw.textsize(char, font=myfont)
    draw.text(((WIDTH - w) / 2,(HEIGHT - h) / 2), char, font=myfont)

    return image

#%% Data import
# Load the data
image_ids0, images0 = load_as_npa("../dataset/bengaliai-cv19/test_image_data_0.parquet")

# Plot some images
f, ax = plt.subplots(3, 1, figsize=(16, 8))
ax = ax.flatten()

for i in range(3):
    ax[i].imshow(images0[i], cmap='Greys')

plt.show()
a = 0