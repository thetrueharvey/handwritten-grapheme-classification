"""
Various data structures needed in the model
"""

#%% Label map
# Create the label map
labels = ( '1'
         , '2'
         , '3'
         , '4'
         , '5'
         , '6'
         )

label_map = {k: v + 1 for v, k in enumerate(labels)}
label_map['background'] = 0

# Create a label colour map
colours = ['#e6194b', '#3cb44b', '#FFFFFF', '#e1334b', '#3cb43b', '#FFFFFF', '#FFFFFF']
colour_map = {k: colours[i] for i, k in enumerate(label_map.keys())}

#%% Feature map dimensions
fmap_dimensions = [38, 19, 10, 5, 3, 1]

#%% Object scales
object_scales = [0.1, 0.2, 0.375, 0.55, 0.725, 0.9]

#%% Aspect ratios
aspect_ratios = [ [1.0, 2.0, 0.5]
                , [1.0, 2.0, 3.0, 0.5, 0.333]
                , [1.0, 2.0, 3.0, 0.5, 0.333]
                , [1.0, 2.0, 3.0, 0.5, 0.333]
                , [1.0, 2.0, 0.5]
                , [1.0, 2.0, 0.5]
                ]


