"""
Custom dataset class
Specifically, builds datasets for a specific feature, ensuring that classes are balanced in the training set
"""
#%% TODO
'''
Add ability to choose specific classes
'''

#%% Setup
import os
import random
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from glob      import glob
from xml.etree import ElementTree
from PIL       import Image


#%% Dataset class
class GraphemeDataset(object):
    def __init__( self
                , image_folder    : str
                , label_file      : str
                , feature         : str
                , balance         : bool  = True
                , train_test_split: float = 0.7
                , train_transforms: list  = None
                , test_transforms : list  = None
                , include_negative: bool  = True
                ):
        """
        Class for creating a dataset for training the flow chart object detector
        """
        # Class attributes
        self.image_folder     = image_folder
        self.feature          = feature
        self.balance          = balance
        self.train_transforms = train_transforms
        self.test_transforms  = test_transforms
        self.include_negative = include_negative

        # Determine which instances to take
        self.labels = pd.read_csv(label_file)

        # Instantiate the classes attribute
        self.classes = self.labels[feature].unique()

        # Create an index list and shuffle
        indices = np.arange(self.labels.shape[0])
        np.random.seed(0)
        np.random.shuffle(indices)

        self.train_idx, self.test_idx = [indices[start:end]
                                         for start,end in ( [0,int(len(indices) * train_test_split)]
                                                          , [int(len(indices) * train_test_split), len(indices)]
                                                          )
                                        ]

        self.train_lists = [self.labels.iloc[self.train_idx].query("{} == {}".format(self.feature, i))
                            for i in self.classes
                           ]

        # Activate training mode
        self.train()

    def __len__(self):
        if self._mode == "train":
            return len(self.train_idx)
        if self._mode == "eval":
            return len(self.test_idx)

    def __getitem__(self, idx):
        if self._mode == "eval" or not self.balance:
            # Load the labels
            label = self.labels.iloc[self._active_list[idx]]

        else:
            # Randomly choose a class and element
            active_list = random.choice(self.train_lists)

            label = active_list.sample(n=1).iloc[0]

        # Load the image
        img = Image.open(self.image_folder + "/" + label["image_id"] + ".png")

        # Determine which class
        cls = label[self.feature]

        # Apply transformations
        if self._active_transforms is not None:
            img_ = self._active_transforms(img)
        else:
            img_ = FT.to_tensor(img)

        return {"image": img_, "label": cls}


    def train(self):
        """
        Activate the training mode of the dataset
        """
        self._mode              = "train"
        self._active_list       = self.train_idx
        self._active_transforms = self.train_transforms

    def eval(self):
        """
        Activate the testing or evaluation mode of the dataset
        """
        self._mode              = "eval"
        self._active_list       = self.test_idx
        self._active_transforms = self.test_transforms
