"""
Custom dataset class
Specifically, builds datasets using subsets of the feature space
"""
#%% TODO
'''
Add ability to choose class balancing (e.g. random uniform sampling to ensure a batch is approximately even across different classes), which is automatically disabled during evaluation
'''

#%% Setup
import os
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from glob      import glob
from xml.etree import ElementTree
from PIL       import Image


#%% Dataset class
class GraphemeDataSubset(object):
    def __init__( self
                , image_folder    : str
                , label_file      : str
                , feature         : str
                , classes         : list
                , train_test_split: float = 0.5
                , train_transforms: list = None
                , test_transforms : list = None
                , include_negative: bool = True
                , balance_classes : bool = True
                ):
        """
        Class for creating a dataset for training the flow chart object detector
        """
        # Class attributes
        self.image_folder     = image_folder
        self.feature          = feature
        self.classes          = classes
        self.train_transforms = train_transforms
        self.test_transforms  = test_transforms
        self.include_negative = include_negative

        # Determine which instances to take
        labels = pd.read_csv(label_file)

        positive_list = labels[labels[self.feature].isin(self.classes)]

        if include_negative:
            n_negative_samples = int(len(positive_list) / len(classes))
            if n_negative_samples > len(labels[~labels[self.feature].isin(self.classes)]):
                n_negative_samples = len(labels[~labels[self.feature].isin(self.classes)])

            negative_list = labels[~labels[self.feature].isin(self.classes)].sample(n=n_negative_samples)

            self.labels = pd.concat([positive_list, negative_list]).sample(frac=1)

        else:
            self.labels = positive_list.sample(frac=1)

        # Create an index list and shuffle
        indices = np.arange(self.labels.shape[0])
        np.random.shuffle(indices)

        self.train_idx, self.test_idx = [indices[start:end]
                                         for start,end in ( [0,int(len(indices) * train_test_split)]
                                                          , [int(len(indices) * train_test_split), len(indices)]
                                                          )
                                        ]

        # Activate training mode
        self.train()

    def __len__(self):
        if self._mode == "train":
            return len(self.train_idx)
        if self._mode == "eval":
            return len(self.test_idx)

    def __getitem__(self, idx):
        # Load the labels
        label = self.labels.iloc[self._active_list[idx]]

        # Load the image
        img = Image.open(self.image_folder + "\\" + label["image_id"] + ".png")

        # Determine which class
        if self.include_negative:
            cls = [i + 1 for i,cls_ in enumerate(self.classes) if cls_ == label[self.feature]]

            if len(cls) == 0:
                cls = [0]
        else:
            cls = [i for i, cls_ in enumerate(self.classes) if cls_ == label[self.feature]]

        # Apply transformations
        if self._active_transforms is not None:
            img = self._active_transforms(img)
        else:
            img = FT.to_tensor(img)

        return {"images":img, "classes":cls[0]}


    def train(self):
        """
        Activate the training mode of the dataset
        """
        self._mode              = "train"
        self._active_list       = self.train_idx
        self._active_transforms = self.train_transforms

    def eval(self):
        """
        Activate the testing or evaluation mode of the dataset
        """
        self._mode              = "eval"
        self._active_list       = self.test_idx
        self._active_transforms = self.test_transforms

