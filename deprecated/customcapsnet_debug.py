"""
Script for testing the custom capsnet for consonant classification
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch

import classification_transforms as T
from deprecated import balanced_dataset as bds

from torchvision      import transforms
from torchtools.optim import RangerLars
from models           import CustomCapsNet
from Loss             import CapsuleLoss

torch.cuda.is_available()

#%% Constants


#%% Dataset
#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.RandomOrder([ T.RandomNoise(decay=5e-6)
                                                     , T.RandomErasures(h=[0.05,0.2], w=[0.05,0.2], decay=5e-6)
                                                     , T.RandomAffine(decay=5e-6)
                                                     , T.RandomCrop(decay=5e-6)
                                                     , T.RandomColourJitter(decay=5e-6)
                                                     , T.Skew()
                                                     , T.RandomDistortion(probability=1)
                                                     ]
                                                    )
                                     , transforms.Grayscale(num_output_channels=1)
                                     , transforms.Resize(size=(128,128))
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ transforms.Grayscale(num_output_channels=1)
                                    , transforms.Resize(size=(128, 128))
                                    , transforms.ToTensor()
                                    ]
                                   )
# Create the dataset
dataset = bds.GraphemeBalancedDataset( image_folder="dataset\\images"
                                     , label_file  ="../dataset/bengaliai-cv19/train.csv"
                                     , feature     ="grapheme_root"
                                     , train_transforms =train_transform
                                     , test_transforms  =test_transform
                                     )
'''
dataset = ds.GraphemeDataSubset( image_folder="dataset\\images"
                               , label_file  ="../dataset/bengaliai-cv19/train.csv"
                               , feature     ="consonant_diacritic"
                               , classes     =[0] #[2,4,5],and [1,3,6]
                               , train_transforms =train_transform
                               , test_transforms  =test_transform
                               , include_negative =True
                               )
'''
# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =50
                                    , shuffle    =True
                                    #, num_workers=3
                                    )

#%% Model
net = CustomCapsNet.CustomCapsNet( n_class    =len(dataset.classes)
                                 , input_shape=[1,128,128]
                                 , add_decoder=False
                                 , name       ="CustomCapsNet root"
                                 )

# Create the loss
#loss_fn = nn.CrossEntropyLoss()
loss_fn = CapsuleLoss( m_plus     =0.9
                     , m_minus    =0.1
                     , lambda_val =0.5
                     , alpha      =0.0005
                     )

# Create the optimizer
optimizer = RangerLars(net.parameters(), lr=1e-2)

# Load state dicts
state_dicts = torch.load("checkpoints\\CustomCapsNet General Weights.tar")

model_state_dict     = state_dicts["net_state_dict"]
#optimizer_state_dict = state_dicts["optimizer_state_dict"]
#epoch                = state_dicts["epoch"]
epoch = 1

# Remove prediction layers
del(model_state_dict["class_caps.W"])

#net.load_state_dict(model_state_dict, strict=False)
#optimizer.load_state_dict(optimizer_state_dict)

net.cuda()

del(state_dicts)
del(model_state_dict)
'''
for state in optimizer.state.values():
    for k, v in state.items():
        if isinstance(v, torch.Tensor):
            state[k] = v.cuda()
'''

# Example input
# Storage DataFrame
train_df = pd.DataFrame()
test_df  = pd.DataFrame()

for epoch in range(epoch,101):
    # Storage
    train_predictions = []
    train_truths      = []
    train_loss        = []

    test_predictions = []
    test_truths      = []
    test_loss        = []

    # Training
    dataset.train()
    net.train()
    for i,sample in enumerate(loader):
        optimizer.zero_grad()

        # Forward pass
        y_pred_ohe, reconstruction, v_length = net(sample["images"].cuda(), sample["classes"].cuda())

        # Predictions
        predictions = np.argmax(y_pred_ohe.cpu(), axis=1)

        # Free memory
        del(y_pred_ohe)

        # Loss
        loss_ = loss_fn(sample["untransformed"], sample["classes"], v_length.cpu(), reconstruction.cpu())
        loss_.backward()
        optimizer.step()

        # Metrics
        train_predictions.append(predictions)
        train_truths.append(sample["classes"].numpy())
        train_loss.append(loss_.detach().item())

        # Print a summary every 100 minibatches
        if (i + 1) % 100 == 0:
            predictions = np.concatenate(train_predictions[-100:])
            truths      = np.concatenate(train_truths[-100:])
            loss        = np.array(train_loss[-100:]).mean()

            accuracy = np.equal(predictions, truths).astype(np.double).mean()

            print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

    # Testing
    dataset.eval()
    net.eval()
    with torch.no_grad():
        for i, sample in enumerate(loader):
            # Forward pass
            y_pred_ohe, reconstruction, v_length = net(sample["images"].cuda(), sample["classes"].cuda())

            # Predictions
            predictions = np.argmax(y_pred_ohe.cpu(), axis=1)

            # Free memory
            del (y_pred_ohe)

            # Loss
            loss_ = loss_fn(sample["untransformed"], sample["classes"], v_length.cpu(), reconstruction.cpu())

            # Metrics
            test_predictions.append(predictions)
            test_truths.append(sample["classes"].numpy())
            test_loss.append(loss_.detach().item())

    # Print a summary of evaluation
    predictions = np.concatenate(test_predictions)
    truths      = np.concatenate(test_truths)
    loss        = np.array(test_loss).mean()

    accuracy = np.equal(predictions, truths).astype(np.double).mean()

    print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

    # Create temporary DataFrames
    # Training
    temp_train_df = pd.DataFrame({ "Epoch"     :epoch
                                 , "Prediction":np.concatenate(train_predictions)
                                 , "Target"    :np.concatenate(train_truths)
                                 }
                                )

    # Testing
    temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                , "Prediction": np.concatenate(test_predictions)
                                , "Target"    : np.concatenate(test_truths)
                                }
                               )

    # Update the full tables
    train_df = pd.concat([train_df, temp_train_df])
    test_df  = pd.concat([test_df, temp_test_df])

    # Save a distinct checkpoint every 5 epochs
    torch.save( { "epoch"               : epoch
                , "net_state_dict"      : net.state_dict()
                , "optimizer_state_dict": optimizer.state_dict()
                , "train_df"            : train_df
                , "test_df"             : test_df
                }
              , "checkpoints\\CustomCapsNet Root Weights.tar"
              )

    # Summarize each epoch
    train_df_summary = temp_train_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    test_df_summary = temp_test_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    # Print a full summary every epoch
    print("Epoch {} training summary:\n".format(epoch))
    print(train_df_summary)

    print("Epoch {} testing summary:\n".format(epoch))
    print(test_df_summary)
