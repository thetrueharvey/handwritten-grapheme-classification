"""
Script for testing models for consonant classification
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch

import transform      as ts
from deprecated import balanced_dataset as bds

from torchvision      import transforms
from torchtools.optim import RangerLars
from models           import MobileCapsNet
from Loss             import CapsuleLoss

torch.cuda.is_available()

#%% Constants


#%% Dataset
#%% Dataset
# Transforms
# Train transform
train_transform = transforms.Compose([ transforms.ToTensor()
                                     , transforms.RandomErasing()
                                     , transforms.ToPILImage()
                                     , transforms.RandomApply([transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5, hue=0.2)])
                                     , transforms.RandomApply([transforms.RandomAffine(degrees=30, translate=(0.4,0.4), scale=(0.5,2), shear=30)])
                                     , ts.Grayscale()
                                     , ts.Resize(dims=(128,128))
                                     , transforms.ToTensor()
                                     , transforms.Normalize((0.0692,), (0.2051,))
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ ts.Grayscale()
                                    , ts.Resize(dims=(128,128))
                                    , transforms.ToTensor()
                                    , transforms.Normalize((0.0692,), (0.2051,))
                                    ]
                                   )
# Create the dataset
dataset = bds.GraphemeBalancedDataset( image_folder="dataset\\images"
                                     , label_file  ="../dataset/bengaliai-cv19/train.csv"
                                     , feature     ="consonant_diacritic"
                                     , train_transforms =train_transform
                                     , test_transforms  =test_transform
                                     )
'''
dataset = ds.GraphemeDataSubset( image_folder="dataset\\images"
                               , label_file  ="../dataset/bengaliai-cv19/train.csv"
                               , feature     ="consonant_diacritic"
                               , classes     =[0] #[2,4,5],and [1,3,6]
                               , train_transforms =train_transform
                               , test_transforms  =test_transform
                               , include_negative =True
                               )
'''
# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =100
                                    , shuffle    =True
                                    #, num_workers=4
                                    , pin_memory =True
                                    )

# Test the dataset
# Fetch a sample
if True:
    dataset.__getitem__(0)

# Benchmarking
if False:
    for sample in tqdm(loader):
        test = sample

# Display some transformed images
if False:
    for sample in loader:
        FT.to_pil_image(sample["images"][0]).show()
        a=0

#%% Model
net = MobileCapsNet.MobileCapsNet( n_class    =len(dataset.classes)
                                 , input_shape=[1,128,128]
                                 , name       ="MobileCapsNet consonant"
                                 )

# Create the loss
#loss_fn = nn.CrossEntropyLoss()
loss_fn = CapsuleLoss( m_plus     =0.9
                     , m_minus    =0.1
                     , lambda_val =0.5
                     , alpha      =0.0005
                     , add_decoder=True
                     )

# Create the optimizer
optimizer = RangerLars(net.parameters())

#state_dicts = torch.load("checkpoints\\MobileCapsNet Weights.tar")
#model_state_dict = state_dicts["net_state_dict"]
#optimizer_state_dict = state_dicts["optimizer_state_dict"]

#net.load_state_dict()

net.cuda()

# Example input
# Storage DataFrame
train_df = pd.DataFrame()
test_df  = pd.DataFrame()

for epoch in range(1,101):
    # Storage
    train_predictions = []
    train_truths      = []
    train_loss        = []

    test_predictions = []
    test_truths      = []
    test_loss        = []

    # Training
    dataset.train()
    net.train()
    for i,sample in enumerate(loader):
        optimizer.zero_grad()

        # Forward pass
        y_pred_ohe, reconstruction, v_length = net(sample["images"].cuda(), sample["classes"].cuda())

        # Predictions
        predictions = np.argmax(y_pred_ohe.cpu(), axis=1)

        # Free memory
        del(y_pred_ohe)

        # Loss
        loss_ = loss_fn(sample["images"], sample["classes"], v_length.cpu(), reconstruction.cpu())
        loss_.backward()
        optimizer.step()

        # Metrics
        train_predictions.append(predictions)
        train_truths.append(sample["classes"].numpy())
        train_loss.append(loss_.detach().item())

        # Print a summary every 100 minibatches
        if (i + 1) % 100 == 0:
            predictions = np.concatenate(train_predictions[-100:])
            truths      = np.concatenate(train_truths[-100:])
            loss        = np.array(train_loss[-100:]).mean()

            accuracy = np.equal(predictions, truths).astype(np.double).mean()

            print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

    # Testing
    dataset.eval()
    net.eval()
    with torch.no_grad():
        for i, sample in enumerate(loader):
            # Forward pass
            y_pred_ohe, reconstruction, v_length = net(sample["images"].cuda(), sample["classes"].cuda())

            # Predictions
            predictions = np.argmax(y_pred_ohe.cpu(), axis=1)

            # Free memory
            del (y_pred_ohe)

            # Loss
            loss_ = loss_fn(sample["images"], sample["classes"], v_length.cpu(), reconstruction.cpu())

            # Metrics
            test_predictions.append(predictions)
            test_truths.append(sample["classes"].numpy())
            test_loss.append(loss_.detach().item())

    # Print a summary of evaluation
    predictions = np.concatenate(test_predictions)
    truths      = np.concatenate(test_truths)
    loss        = np.array(test_loss).mean()

    accuracy = np.equal(predictions, truths).astype(np.double).mean()

    print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

    # Create temporary DataFrames
    # Training
    temp_train_df = pd.DataFrame({ "Epoch"     :epoch
                                 , "Prediction":np.concatenate(train_predictions)
                                 , "Target"    :np.concatenate(train_truths)
                                 }
                                )

    # Testing
    temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                , "Prediction": np.concatenate(test_predictions)
                                , "Target"    : np.concatenate(test_truths)
                                }
                               )

    # Update the full tables
    train_df = pd.concat([train_df, temp_train_df])
    test_df  = pd.concat([test_df, temp_test_df])

    # Save a distinct checkpoint every 5 epochs
    torch.save( { "epoch"               : epoch
                , "net_state_dict"      : net.state_dict()
                , "optimizer_state_dict": optimizer.state_dict()
                , "train_df"            : train_df
                , "test_df"             : test_df
                }
              , "checkpoints\\MobileCapsNet Weights.tar"
              )

    # Summarize each epoch
    train_df_summary = temp_train_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    test_df_summary = temp_test_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    # Print a full summary every epoch
    print("Epoch {} training summary:\n".format(epoch))
    print(train_df_summary)

    print("Epoch {} testing summary:\n".format(epoch))
    print(test_df_summary)

