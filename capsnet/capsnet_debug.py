"""
Testing of an implementation of Capsule Networks
Based on: https://github.com/higgsfield/Capsule-Network-Tutorial/blob/master/Capsule%20Network.ipynb
and: https://github.com/hula-ai/capsule_network_dynamic_routing/blob/master/capsnet_v2.py
"""
#%% Setup
import numpy as np

import torch
import torch.nn            as nn
import torch.nn.functional as F

from torchtools.optim import RangerLars

from torchvision import datasets, transforms

#%% Create the dataset
class MNISTDataset:
    def __init__(self, batch_size):
        # Create the training transforms
        transform = transforms.Compose([ transforms.ToTensor()
                                       , transforms.RandomErasing()
                                       , transforms.ToPILImage()
                                       , transforms.RandomApply([ transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5, hue=0.2)
                                                                , transforms.RandomAffine(degrees=30, translate=(0.4,0.4), scale=(0.5,2), shear=30)
                                                                ]
                                                               )
                                       , transforms.ToTensor()
                                       , transforms.Normalize((0.1307,), (0.3081,))
                                       ]
                                      )

        # Create the datasets
        train_dataset = datasets.MNIST( "../data"
                                      , train    =True
                                      , download =True
                                      , transform=transform
                                      )
        test_dataset  = datasets.MNIST( "../data"
                                      , train    =False
                                      , download =True
                                      , transform=transforms.Compose([ transforms.ToTensor()
                                                                     , transforms.Normalize((0.1307,), (0.3081,))
                                                                     ]
                                                                    )
                                      )

        # Create the loaders
        self.train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        self.test_loader  = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

# %% Utility functions
# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))

# Squash
def squash(x, dim=-1, epsilon=1e-7):
    # Determine the normal vector, and account for 0 values
    squared_norm = (x ** 2).sum(dim=dim, keepdim=True)
    safe_norm    = torch.sqrt(squared_norm + epsilon)

    # Determine the vector length and direction
    scale       = squared_norm / (1 + squared_norm)
    unit_vector = x / safe_norm

    return scale * unit_vector

#%% Network layers
class ConvLayer(nn.Module):
    def __init__(self, in_channels=1, out_channels=256, kernel_size=9):
        super(ConvLayer, self).__init__()

        # Create the convolutions
        self.conv = nn.ModuleList([ nn.Conv2d( in_channels =in_channels
                                             , out_channels=out_channels
                                             , kernel_size =kernel_size
                                             , padding     =1
                                             , stride      =1
                                             )
                                  , nn.Conv2d( in_channels =out_channels
                                             , out_channels=out_channels
                                             , kernel_size =kernel_size
                                             , stride      =1
                                             , padding     =1
                                             )
                                  , nn.Conv2d( in_channels =out_channels
                                             , out_channels=out_channels
                                             , kernel_size =kernel_size
                                             , stride      =1
                                             , padding     =1
                                             )
                                  ]
                                 )

        self.conv_pool = nn.Conv2d( in_channels =out_channels
                                  , out_channels=out_channels
                                  , kernel_size =kernel_size
                                  , stride      =2
                                  , padding     =1
                                  )

    def forward(self, x):
        for layer in self.conv:
            x = Mish(layer(x))

        x = Mish(self.conv_pool(x))

        return x

class PrimaryCapsules(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, capsule_dim, num_cap_map):
        super(PrimaryCapsules, self).__init__()

        # Class attributes
        self.capsule_dim = capsule_dim
        self.num_cap_map = num_cap_map

        # Capsule Convolution
        self.conv = nn.Conv2d( in_channels =in_channels
                             , out_channels=out_channels
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =0
                             )

    def forward(self, x):
        batch_size = x.size(0)
        x = self.conv(x)

        map_dim = x.size(-1)
        x = x.view(batch_size, self.capsule_dim, self.num_cap_map, map_dim, map_dim)
        x = x.view(batch_size, self.capsule_dim, -1).transpose(-1, -2)

        return squash(x)


class DigitCapsules(nn.Module):
    def __init__(self, num_digit_cap, num_prim_cap, in_cap_dim, out_cap_dim, num_iterations):
        super(DigitCapsules, self).__init__()

        # Class attributes
        self.num_digit_cap  = num_digit_cap
        self.num_prim_cap   = num_prim_cap
        self.num_iterations = num_iterations

        self.W = nn.Parameter(0.01 * torch.randn(1, num_digit_cap, 800, out_cap_dim, in_cap_dim))

    def forward(self, x):
        batch_size = x.size(0)

        u     = x.unsqueeze(1).unsqueeze(4)
        u_hat = torch.matmul(self.W, u)
        u_hat = u_hat.squeeze(-1)

        # Detach u_hat during routing (prevents gradient flow)
        u_hat_ = u_hat.detach()

        # Routing
        b = torch.zeros(batch_size, self.num_digit_cap, 800, 1).cuda()
        for i in range(self.num_iterations - 1):
            c = F.softmax(b, dim=1)
            s = (c * u_hat_).sum(dim=2)
            v = squash(s)

            uv = torch.matmul(u_hat_, v.unsqueeze(-1))
            b += uv

        # Final routing
        c = F.softmax(b, dim=1)
        s = (c * u_hat).sum(dim=2)

        return squash(s)

class Decoder(nn.Module):
    def __init__(self, num_classes, height, width):
        super(Decoder, self).__init__()

        # Create the fully connected decoder layers
        self.reconstruction_layers = nn.ModuleList([ nn.Linear(in_features=16*num_classes, out_features=512)
                                                   , nn.Linear(in_features=512, out_features=1024)
                                                   , nn.Linear(in_features=1024, out_features=height*width)
                                                   ]
                                                  )

    def forward(self, x, y):
        x = (x * y[:, :, None].float()).view(x.size(0), -1)
        for layer, activation in zip(self.reconstruction_layers, (Mish, Mish, torch.sigmoid)):
            x = activation(layer(x))

        return x

#%% Loss
class CapsuleLoss(nn.Module):
    def __init__(self, m_plus, m_minus, lambda_val, alpha, add_decoder):
        super(CapsuleLoss, self).__init__()

        # Class attributes
        self.m_plus      = m_plus
        self.m_minus     = m_minus
        self.lambda_val  = lambda_val
        self.alpha       = alpha
        self.add_decoder = add_decoder

    def forward(self, images, labels, v_c, reconstructions):
        present_error = F.relu(self.m_plus - v_c, inplace=True) ** 2
        absent_error  = F.relu(v_c - self.m_minus, inplace=True) ** 2

        l_c = labels.float() * present_error + self.lambda_val * (1.0 - labels.float()) * absent_error
        margin_loss = l_c.sum(dim=1).mean()

        reconstruction_loss = 0
        if self.add_decoder:
            assert torch.numel(images) == torch.numel(reconstructions)
            images = images.view(reconstructions.size(0), -1)
            reconstruction_loss = torch.mean((reconstructions - images) ** 2)

        return margin_loss + self.alpha * reconstruction_loss

#%% Model
class CapsNet(nn.Module):
    def __init__(self, num_classes, conv_layers, primary_capsules, digit_capsules, decoder=None):
        super(CapsNet, self).__init__()

        # Class attributes
        self.num_classes      = num_classes
        self.conv_layers      = conv_layers
        self.primary_capsules = primary_capsules
        self.digit_capsules   = digit_capsules
        self.decoder          = decoder

    def forward(self, x, y=None):
        reconstruction = torch.zeros_like(x)

        # Forward pass through the model layers
        for cell in self.conv_layers:
            x = cell(x)

        x = self.primary_capsules(x)
        x = self.digit_capsules(x)

        v_length = torch.norm(x, dim=-1)

        _, y_pred  = v_length.max(dim=1)
        y_pred_ohe = F.one_hot(y_pred, self.num_classes)

        if y is None:
            y = y_pred_ohe

        if self.decoder is not None:
            reconstruction = self.decoder(x, y)

        return y_pred_ohe, reconstruction, v_length

#%% Component setup
# Image dimensions
image_dims = [1,28,28]

# Initial convolution
conv_layer_1 = ConvLayer( in_channels =1
                        , out_channels=64
                        , kernel_size =3
                        )
conv_layer_2 = ConvLayer( in_channels =64
                        , out_channels=256
                        , kernel_size =3
                        )

conv_layers = nn.ModuleList([conv_layer_1, conv_layer_2])

# Capsules
# Primary capsules
primary_capsules = PrimaryCapsules( in_channels =conv_layers[-1].conv[-1].out_channels
                                  , out_channels=256
                                  , kernel_size =3
                                  , stride      =1
                                  , capsule_dim =8
                                  , num_cap_map =int(256 / 8)
                                  )

num_primary_cap = int(((image_dims[1] - 2*(primary_capsules.conv.kernel_size[0] - 1)) ** 2) / (primary_capsules.conv.stride[0]) ** 2)

# Digits capsules
digit_capsules = DigitCapsules( num_digit_cap =10
                              , num_prim_cap  =primary_capsules.num_cap_map * num_primary_cap
                              , in_cap_dim    =primary_capsules.capsule_dim
                              , out_cap_dim   =16
                              , num_iterations=3
                              )

# Decoder
decoder = Decoder( num_classes=digit_capsules.num_digit_cap
                 , height     =image_dims[1]
                 , width      =image_dims[2]
                 )

#%% Network
net = CapsNet( num_classes     =digit_capsules.num_digit_cap
             , conv_layers     =conv_layers
             , primary_capsules=primary_capsules
             , digit_capsules  =digit_capsules
             , decoder         =decoder
             )

net.cuda()

#%% Loss
loss = CapsuleLoss( m_plus     =0.9
                  , m_minus    =0.1
                  , lambda_val =0.5
                  , alpha      =0.0005
                  , add_decoder=True
                  )

#%% Optimizer
optimizer = RangerLars(params=net.parameters())

#%% Dataset
dataset = MNISTDataset(batch_size=200)

#%% Training
for epoch in range(1,101):
    # Storage
    train_accuracy = []
    test_accuracy  = []

    # Training
    net.train()
    for (images, labels) in dataset.train_loader:
        optimizer.zero_grad()

        # One-hot encode the targets
        labels = F.one_hot(labels, digit_capsules.num_digit_cap)

        y_pred_ohe, reconstruction, v_length = net(images.cuda(), labels.cuda())

        loss_ = loss(images, labels, v_length.cpu(), reconstruction.cpu())

        loss_.backward()
        optimizer.step()

        # Accuracy
        accuracy = np.equal(np.argmax(labels, axis=1), np.argmax(y_pred_ohe.cpu(), axis=1)).float().mean()

        #print("Training Loss: {} Training Accuracy: {}".format(loss_.item(), accuracy))

        # Metrics
        train_accuracy.append(accuracy)

    # Evaluation
    net.eval()
    with torch.no_grad():
        for (images, labels) in dataset.test_loader:
            # One-hot encode the targets
            labels = F.one_hot(labels, digit_capsules.num_digit_cap)

            y_pred_ohe, reconstruction, v_length = net(images.cuda(), labels.cuda())

            loss_ = loss(images, labels, v_length.cpu(), reconstruction.cpu())

            # Accuracy
            accuracy = np.equal(np.argmax(labels, axis=1), np.argmax(y_pred_ohe.cpu(), axis=1)).float().mean()

            #print("Testing Loss: {} Testing Accuracy: {}".format(loss_.item(), accuracy))

            # Metrics
            test_accuracy.append(accuracy)

    # Epoch averages
    print("Training accuracy: {} Testing accuracy: {}".format(torch.stack(train_accuracy).mean(), torch.stack(test_accuracy).mean()))

    a=0
