"""
Performs all necessary dataset preprocessing
"""
#%% Setup
import os
import shutil
import numpy  as np
import pandas as pd

from glob import glob
from tqdm import tqdm
from PIL  import Image

#%% Constants
ROOT_FOLDER = "dataset/images/"

#%% Utility Functions
#%% Constants
HEIGHT = 137
WIDTH  = 236

#%% Functions
# Bounding Box
def bbox(image):
    """
    Determines the bounding boxes for images to remove empty space where possible
    :param image:
    :return:
    """
    for i in range(image.shape[1]):
        if (image[:,i] > 0).sum() >= 5:
            x_min = i - 8 if (i > 8) else 0
            break

    for i in reversed(range(image.shape[1])):
        if (image[:,i] > 0).sum() >= 1:
            x_max = i + 8 if (i < WIDTH - 8) else WIDTH
            break

    for i in range(image.shape[0]):
        if (image[i] > 0).sum() >= 5:
            y_min = i - 8 if (i > 8) else 0
            break

    for i in reversed(range(image.shape[0])):
        if (image[i] > 0).sum() >= 5:
            y_max = i + 8 if (i < HEIGHT - 8) else HEIGHT
            break

    return x_min, y_min, x_max, y_max

#%%
# Delete existing images
try:
    shutil.rmtree(ROOT_FOLDER)
except:
    pass
os.mkdir(ROOT_FOLDER)

# Loop through each dataset
for fl in tqdm(glob("dataset/bengaliai-cv19/train_image_data_*.parquet")):
    # Load the dataset
    df = pd.read_parquet(fl)

    # Fetch the images and IDs
    ids, images = df.iloc[:, 0], df.iloc[:, 1:].values.reshape(-1, HEIGHT, WIDTH)
    del(df)

    # Save each image
    for id,image in tqdm(zip(ids, images)):
        # Input data is inverted
        image_ = 255 - image

        # Remove the boundaries of the image
        image_ = image_[5:-5, 5:-5]

        # Remove los intensity pixels
        image_[image_ < 20] = 0

        # Apply max normalization
        image_ = (image_ * (255.0 / image_.max())).astype(np.uint8)

        # Crop the image
        x_min, y_min, x_max, y_max = bbox(image_)
        image_ = image_[y_min:y_max, x_min:x_max]

        image_ = Image.fromarray(image_)

        # Save
        image_.save("dataset\\images\\{}.png".format(id))
