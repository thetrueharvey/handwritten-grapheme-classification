"""
Class for creating the necessary transforms for the VOC dataset
"""
# imports
from random import uniform, randint, shuffle, random
from PIL import Image
import torch
import torchvision.transforms.functional as FT

class Transform(object):
    def __init__( self
                , transforms:list
                , mean      :tuple       =[0.5]
                , std       :tuple       =[0.225]
                , norm      :bool        = True
                ):
        """
        Composes various transforms
        :param transforms: transforms to apply
        :param mean: list(R,G,B). Mean values of each channel, for normalisation
        :param std: list(R,G,B). Standard devation values of each channel, for normalisation
        """
        # store transform attributes
        self.transforms = transforms
        self.mean       = mean
        self.std        = std
        self.norm       = norm

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Applies the transforms
        :param image: PIL Image. The image file to be transformed
        :param boxes: The boxes of the objects in the image
        :param labels: The labels associated with the objects in the image
        :return: Tensor. The transformed image, cast to a tensor
        """
        # handle image only transforms
        if boxes is None and labels is None:
            image_new = image

            image_new = self._image_only(image)

            if self.norm:
                image_new = FT.normalize(image_new, mean=self.mean, std=self.std)

            return image_new

        # instantiate the new objects
        image_new  = image
        boxes_new  = boxes
        labels_new = labels

        # loop through each transform
        for transform in self.transforms:
            image_new, boxes_new, labels_new = transform(image_new, boxes_new, labels_new)

        # convert the image to a tensor
        image_new = FT.to_tensor(image_new)

        # normalize
        if self.norm:
            image_new = FT.normalize(image_new, mean=self.mean, std=self.std)

        return image_new, boxes_new, labels_new

    def _image_only(self, image:Image):
        """
        Applies transforms only to an image. Intended for evaluation / deployment purposes
        :param image: PIL Image. The image file to be transformed
        :return: Tensor. The transformed image, cast to a tensor
        """
        # instantiate the new image
        image_new = image

        # loop through each transform
        for transform in self.transforms:
            image_new = transform(image_new)

        # convert the image to a tensor
        image_new = FT.to_tensor(image_new)

        # normalize
        image_new = FT.normalize(image_new, mean=self.mean, std=self.std)

        return image_new

class Expand(object):
    def __init__( self
                , filler    : tuple
                , max_scale :float  =4
                , chance    :float  =0.5
                ):
        """
        Transform for expanding an image
        Helps with learning to detect smaller objects
        The original image is placed in a canvas of filler material
        :param filler: tuple[R,G,B]. The value to fill the missing image area with, for each channel
        :param max_scale: float. The maximum amount by which the image may be scaled
        :param chance: float. The probability of applying this transform
        """
        # store transform attributes
        self.filler    = filler
        self.max_scale = max_scale
        self.chance    = chance

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Performs the transform on the image
        :param image: PIL Image. The image file to be transformed
        :param boxes: The boxes of the objects in the image
        :return: PIL Image. The zoomed image
        """
        # check if this transform is applied
        if random() > self.chance:
            if boxes is None and labels is None:
                return image

            return image, boxes, labels

        # calculate the scaling factor
        scale = uniform(1, self.max_scale)

        # calculate the new dimensions
        x_new, y_new = [image.size[dim] * scale for dim in (0, 1)]

        # create a filler image
        image_new = torch.ones((3, int(y_new), int(x_new)), dtype=torch.float32) \
                    * torch.FloatTensor(self.filler).unsqueeze(1).unsqueeze(1)

        # get random coordinates
        x_start, y_start = [randint(0, int(dim_new - image.size[dim])) for dim_new, dim in zip((x_new, y_new), (0, 1))]
        x_end, y_end   = [int(start + image.size[dim]) for start, dim in zip((x_start, y_start), (0, 1))]

        # place the original image at the coordinates
        image_new[:, y_start:y_end, x_start:x_end] = FT.to_tensor(image)

        # Return only the image
        if boxes is None and labels is None:
            return FT.to_pil_image(image_new)

        # adjust the bounding box coordinates
        boxes_new = boxes + torch.FloatTensor([x_start, y_start, x_start, y_start]).unsqueeze(0)

        return FT.to_pil_image(image_new), boxes_new, labels

class Crop(object):
    def __init__( self
                , min_scale   :float =0.3
                , attempts    :int   =50
                , aspect_ratio:tuple =(0.5,2)
                , chance      :float =0.5
                ):
        """
        Transform for cropping an image randomly
        Helps with learning to detect larger and partial objects
        :param min_scale: float. Minimum permissible cropped dimensions ratio
        :param attempts: int. Number of times to try crop the image
        :param aspect_ratio: tuple(min, max). Permissible boundary conditions for resulting image
        :param chance: float. The probability of applying this transform
        """
        # store transform attributes
        self.min_scale    = min_scale
        self.attempts     = attempts
        self.aspect_ratio = aspect_ratio
        self.chance       = chance

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Performs the transform on the image
        :param image: PIL Image. The image file to be transformed
        :param boxes: The boxes of the objects in the image
        :param labels: The labels associated with the objects in the image
        :return: PIL Image. The cropped image
        """
        # Check if this transform is applied
        if random() > self.chance:
            return image, boxes, labels

        # iterate until the image is cropped successfully
        while True:
            # get a random minimum overlap
            min_overlap = randint(0,10) / 10.0

            # if 1, then return everything
            if min_overlap == 1:
                return image, boxes, labels

            # try to crop the image
            for _ in range(self.attempts):
                # get the x and y crops
                crop_x, crop_y = [uniform(self.min_scale, 1) for _ in range(2)]

                # hence calculate new height and width
                x_new, y_new = [crop * image.size[dim] for crop, dim in zip((crop_x, crop_y), (0, 1))]

                # check the aspect ratio
                if not self.aspect_ratio[0] < x_new / y_new < self.aspect_ratio[1]:
                    continue

                # new image coordinates
                x_start, y_start = [randint(0, int(image.size[dim] - dim_new)) for dim_new, dim in zip((x_new, y_new), (0, 1))]
                x_end, y_end = [start + image.size[dim] for start, dim in zip((x_start, y_start), (0, 1))]

                # define the crop
                crop = torch.FloatTensor([x_start, y_start, x_end, y_end])

                # get the overlaps of the boxes with the new crop
                overlap = multi_IoU(crop.unsqueeze(0)
                                   , boxes
                                   ).squeeze(0)

                # check that at least one box overlaps sufficiently
                if overlap.max().item() < min_overlap:
                    continue

                # crop the image
                image_new = FT.to_tensor(image)[:, y_start:y_end, x_start:x_end]

                # get the original box centers
                centers = (boxes[:, :2] + boxes[:, 2:]) / 2

                # find boxes with centers still in the cropped region
                valid_centers = (centers[:, 0] > x_start) * (centers[:, 0] < x_end) * (centers[:, 1] > y_start) * (centers[:, 1] < y_end)

                # check the valid centers
                if not valid_centers.any():
                    continue

                # discard samples that do not have centers in frame
                boxes_new = boxes[valid_centers, :]
                labels_new = labels[valid_centers]

                # calculate the new box coordinates
                boxes_new[:, :2] = torch.max(boxes_new[:, :2], crop[:2])
                boxes_new[:, :2] -= crop[:2]
                boxes_new[:, 2:] = torch.min(boxes_new[:, 2:], crop[2:])
                boxes_new[:, 2:] -= crop[:2]

                # return the cropped samples
                return FT.to_pil_image(image_new), boxes_new, labels_new

class Flip(object):
    def __init__( self
                , chance :float =0.5
                ):
        """
        Flips an image horizontally
        :param chance: float. The probability of applying this transform
        """
        # store transform attributes
        self.chance = chance

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Flips an image horizontally
        :param image: PIL Image. The image file to be transformed
        :param boxes: The boxes of the objects in the image
        :return: PIL Image. The flipped image
        """
        # check if this transform is applied
        if random() > self.chance:
            return image, boxes, labels

        # flip the image
        image_new = FT.hflip(image)

        # flip the boxes
        boxes_new = boxes
        boxes_new[:, 0] = image.width - boxes[:, 0] - 1
        boxes_new[:, 2] = image.width - boxes[:, 2] - 1
        boxes_new = boxes_new[:, [2,1,0,3]]

        return image_new, boxes_new, labels

class Resize(object):
    def __init__( self
                , dims          :tuple=(500,500)
                , percent_coords:bool =True
                ):
        """
        Resizes an image to the desired dimensions
        :param dims: tuple(w,h). Desired dimensions of the resized image
        :param percent_coords: bool. Whether to return the boxes as % values
        :param chance: float. The probability of applying this transform
        """
        # store transform attributes
        self.dims           = dims
        self.percent_coords = percent_coords

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Resizes an image
        :param image: PIL Image. The image file to be transformed
        :param boxes: The boxes of the objects in the image
        :return: PIL Image. The resized image
        """
        # handle image only transforms
        if boxes is None and labels is None:
            return self._image_only(image)

        # resize the image
        image_new = FT.resize(image, self.dims)

        # new bounding boxes as a fraction
        boxes_new = boxes / torch.FloatTensor([image.width, image.height, image.width, image.height]).unsqueeze(0)

        # convert to absolute coordinates if necessary
        if not self.percent_coords:
            boxes_new = boxes_new * torch.FloatTensor([dims[1], dims[0], dims[1], dims[0]]).unsqueeze(0)

        return image_new, boxes_new, labels

    def _image_only(self, image:Image):
        """
        Applies resizing only to an image. Intended for evaluation / deployment purposes
        :param image: PIL Image. The image file to be transformed
        :return: Tensor. The transformed image, cast to a tensor
        """
        # resize the image
        image_new = FT.resize(image, self.dims)

        return image_new

class PhotometricDistort(object):
    def __init__( self
                , distortions:list=( FT.adjust_brightness
                                   , FT.adjust_contrast
                                   , FT.adjust_saturation
                                   , FT.adjust_hue
                                   )
                , chance     :float= 0.5
                ):
        """
        Applies random photometric distortions to an image
        These include brightness, contrast, saturation and hue
        :param distortions: tuple. A tuple of TorchVision functional transforms
        :param chance: float. The probability of a distortion being applied
        """
        # store transform attributes
        self.distortions = list(distortions)
        self.chance      = chance

        # shuffle the distortions
        shuffle(self.distortions)

    def __call__(self, image:Image, boxes=None, labels=None):
        """
        Photometrically distorts an image
        :param image: PIL Image. The image file to be transformed
        :return: PIL Image. The distorted image
        """
        image_new = image

        # loop through each distortion and apply it randomly
        for d in self.distortions:
            if random() < self.chance:
                # hue must be controlled specially
                if d.__name__ is 'adjust_hue':
                    adjust_factor = uniform(-18 / 255., 18 / 255.)
                else:
                    adjust_factor = uniform(0.5, 1.5)

                # apply distortion
                image_new = d(image_new, adjust_factor)

        # Return only the image
        if boxes is None and labels is None:
            return image_new

        return image_new, boxes, labels

class Grayscale(object):
    def __init__( self
                , chance: float = 1.0
                , out_channels  = 1
                ):
        """
        Converts an input image to grayscale
        :param chance:
        :param out_channels:
        """
        self.chance       = chance
        self.out_channels = out_channels

    def __call__(self, image:Image, boxes=None, labels=None):
        image_new = image

        if random() < self.chance:
            image_new = FT.to_grayscale(image, num_output_channels=self.out_channels)

        # Return only the image
        if boxes is None and labels is None:
            return image_new

        return image_new, boxes, labels

# IoU calculation
def multi_IoU(box_set_1:torch.FloatTensor, box_set_2:torch.FloatTensor):
    """
    Calculates the IoU of one set of boxes against another set of boxes
    :param box_set_1: Tensor. One set of boxes
    :param box_set_2: Tensor. Another set of boxes
    :return: Tensor. An IoU score for the first set against the second
    """
    # get the intersection of the boxes
    intersection = find_intersection(box_set_1, box_set_2)

    # calculate the area of each box in each set
    areas_1, areas_2 = [(x[:, 2] - x[:, 0]) * (x[:, 3] - x[:, 1]) for x in (box_set_1, box_set_2)]

    # calculate the union area of each combination of boxes
    union = areas_1.unsqueeze(1) + areas_2.unsqueeze(0) - intersection

    # calculate IoU
    IoU = intersection / union

    return IoU

# Box intersection
def find_intersection(box_set_1:torch.FloatTensor, box_set_2:torch.FloatTensor):
    """
    Calculates the area of intersections of one set of boxes against another set of boxes
    :param box_set_1: Tensor. One set of boxes
    :param box_set_2: Tensor. Another set of boxes
    :return: Tensor. Intersection of each box in set 1 against each box in set 2
    """
    # get the upper and lower bounds for each box
    lower_bounds = torch.max(box_set_1[:, :2].unsqueeze(1), box_set_2[:, :2].unsqueeze(1))
    upper_bounds = torch.min(box_set_1[:, 2:].unsqueeze(1), box_set_2[:, 2:].unsqueeze(1))

    # get the intersection dimensions
    intersection_dims = torch.clamp(upper_bounds - lower_bounds, min=0)

    # reduce dimensionality
    intersection_dims = intersection_dims[:,:,0] * intersection_dims[:,:,1]

    return intersection_dims

