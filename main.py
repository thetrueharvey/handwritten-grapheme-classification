"""
Testing and debugging of Inception Resnet V2 Model
"""
#%% Setup

import numpy  as np
import pandas as pd

import torch
import torch.nn            as nn
import torch.nn.functional as F

import classification_transforms as T
import Dataset                   as ds
import utils

from torchvision      import transforms
from torchtools.optim import RangerLars
from models           import SEResNeXt101, MobileNetV2
from Loss             import CrossEntropyHardNegative

from models.SEResNeXt101 import SeResNeXt101
from models.MobileNetV2  import MobileNetV2

torch.cuda.is_available()

#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.RandomDistortion(probability=0.8, grid=[4,4], magnitude=7)
                                     , T.RandomErasures(n_erasures=[1,2], h=[0.2, 0.4], w=[0.2, 0.4], decay=1e-5, decay_stop=1.0, probability=0.9)
                                     , T.RandomAffine(angle=[-7,7], decay=1e-5, decay_stop=1.0, probabilities=[0.8,0.8,0.8,0.8,0.8,0.8])
                                     , T.RandomNoise(decay=1e-5, decay_stop=1.0, probability=0.5)
                                     #, T.RandomCrop(decay=1e-7, decay_stop=0.3, probability=0.5)
                                     #, T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.2,0.2,0.2,0.2,0.2])
                                     #, T.Skew(probability=0.1)
                                     , transforms.Grayscale(num_output_channels=1)
                                     , transforms.Resize((92, 116))
                                     , transforms.ToTensor()
                                     , transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ transforms.Grayscale(num_output_channels=1)
                                    , transforms.Resize((92, 116))
                                    , transforms.ToTensor()
                                    , transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                                    ]
                                   )

# Create the dataset
dataset = ds.GraphemeDataset( image_folder="dataset/images"
                            , label_file  ="dataset/images/train.csv"
                            , feature     ="consonant_diacritic"
                            , balance     =False
                            , train_transforms=train_transform
                            , test_transforms =test_transform
                            )

# Fit to determine statistics
#metrics = dataset.fit()

# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =512
                                    , shuffle    =True
                                    #, num_workers=4
                                    #, pin_memory =True
                                    )

#%% Network setup
#net = SeResNeXt101(n_class=len(dataset.classes), input_shape=[1,128,128])
net = MobileNetV2(n_class=len(dataset.classes), input_shape=[1,92,116])

# Load state dicts
# General warm starting
#model_state_dict = torch.load("checkpoints\\MobileNetV2 Best Weights.tar")["model_state_dict"]

#del(model_state_dict["layers.19.weight"])
#del(model_state_dict["layers.19.bias"])

# Resume training
#model_state_dict = torch.load("checkpoints\\MobileNetV2 - Vowel Best Weights.tar")["model_state_dict"]
#model_state_dict = torch.load("checkpoints\\MobileNetV2 - Consonant Best Weights.tar")["model_state_dict"]

# Resume from checkpoint
#net.load_state_dict(model_state_dict, strict=False)
net.cuda()

# Create the loss
#loss_fn = nn.CrossEntropyLoss()
loss_fn = CrossEntropyHardNegative(ratio=0.5)

# Create the optimizer
optimizer = RangerLars(net.parameters())
#optimizer = torch.optim.Adagrad(params=net.parameters(), lr=2e-3)
#optimizer = torch.optim.RMSprop(params=net.parameters())

scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( optimizer=optimizer
                                                      , mode     ="max"
                                                      , factor   =0.2
                                                      , patience =2
                                                      , verbose  =1
                                                      , min_lr   =1e-8
                                                      , cooldown =0
                                                      )
'''
scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0 = len(loader), T_mult =2, eta_min=1e-6)                                                   

scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=0.5, steps_per_epoch=len(loader), epochs=100)
'''
#%% Training
best_acc, epoch = utils.train( name          ="MobileNetV2 - Vowel"
                             , dataset       =dataset
                             , loader        =loader
                             , net           =net
                             , loss_fn       =loss_fn
                             , optimizer     =optimizer
                             , scheduler     =scheduler
                             , scheduler_type="per epoch"
                             , epoch_end     =100
                             , early_stopping=50
                             , half          =True
                             , save_dir      ="checkpoints"
                             )

# %% Saving
if True:
    for name,checkpoint,model,classes in zip( ["root", "vowel", "consonant"]
                                            , [ "checkpoints/SeResNeXt101 - Root Best Weights.tar"
                                              , "checkpoints/MobileNetV2 - Vowel Best Weights.tar"
                                              , "checkpoints/MobileNetV2 - Consonant Best Weights.tar"
                                              ]
                                            , [SeResNeXt101, MobileNetV2, MobileNetV2]
                                            , [168, 11, 7]
                                            ):
        # Instantiate a model
        net = model(n_class=classes, input_shape=[1, 128, 128])

        # Load the state dict
        net.load_state_dict(torch.load(checkpoint)["model_state_dict"])

        # Save only the weights
        torch.save(net.state_dict(), "dataset/submission/models/" + name + ".pth")

