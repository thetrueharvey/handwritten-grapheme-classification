"""
Custom dataset class
"""

#%% Setup
import os
import random
import torch
import torchvision.transforms.functional as FT

import numpy  as np
import pandas as pd

from glob      import glob
from xml.etree import ElementTree
from PIL       import Image


#%% Dataset class
class GraphemeDataset(object):
    def __init__( self
                , image_folder: str
                , label_df    : str
                , transforms  : list  = None
                ):
        """
        Dataset class
        """
        # Class attributes
        self.image_folder = image_folder
        self.labels       = label_df
        self.transforms   = transforms

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        # Fetch the label
        label = self.labels.iloc[idx]

        # Load the image
        img = Image.open(self.image_folder + "/" + label["image_id"] + ".png")

        # Fetch the id
        id_ = label["image_id"]

        # Format the label
        label = [label["grapheme_root"], label["vowel_diacritic"], label["consonant_diacritic"]]

        # Apply transformations
        if self.transforms is not None:
            img_ = self.transforms(img)
        else:
            img_ = FT.to_tensor(img)

        return {"image": img_, "label": label, "id":id_}

    def fit(self):
        """
        Calculates the various metrics of the dataset (dimensions, pixel values)
        :return:
        """
        heights = []
        widths  = []
        values  = []

        # Loop through the training set
        for i,row in self.labels.itertuples():
            img = np.array(Image.open(self.image_folder + "/" + self.row["image_id"] + ".png"), dtype="uint8")

            # Update storage
            heights.append(img.shape[0])
            widths.append(img.shape[1])
            values.append(img.reshape(-1,1))

        values = np.concatenate(values)

        return { "height":int(np.array(heights).mean())
               , "width" :int(np.array(widths).mean())
               , "mean":np.array(values).mean()
               , "sd":np.array(values).std()
               }

