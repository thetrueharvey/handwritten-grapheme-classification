"""
Testing and debugging of PASNet-5 Model
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

import transform      as ts
from deprecated import balanced_dataset as bds

from torchtools.optim import RangerLars
from models import PNASNet_5_Large as pnet
from Loss   import CrossEntropyHardNegative

torch.cuda.is_available()

#%% Dataset
# Transforms
# Train transform
train_transform = ts.Transform( transforms=[ ts.Expand(filler=(1, 1, 1), max_scale=2, chance=0.5)
                                           , ts.Grayscale()
                                           #, ts.Flip(chance=0.5)
                                           , ts.Resize(dims=(128,128))
                                           , ts.PhotometricDistort(chance=0.5)

                                           ]
                              , mean=[0.0692]
                              , std =[0.2051]
                              )

# Test transform
test_transform = ts.Transform( transforms=[ ts.Resize(dims=(128,128))
                                          , ts.Grayscale()
                                          ]
                             , mean=[0.0692]
                             , std =[0.2051]
                             )

# Create the dataset

dataset = bds.GraphemeBalancedDataset( image_folder="dataset\\images"
                                     , label_file  ="../dataset/bengaliai-cv19/train.csv"
                                     , feature     ="grapheme_root"
                                     , train_transforms =train_transform
                                     , test_transforms  =test_transform
                                     )
'''
dataset = ds.GraphemeDataSubset( image_folder="dataset\\images"
                               , label_file  ="dataset\\bengaliai-cv19\\train.csv"
                               , feature     ="vowel_diacritic"
                               , classes     = [0, 1] #[5,6,8,10] [2,3,4,7,9]
                               , train_transforms =train_transform
                               , test_transforms  =test_transform
                               , include_negative =True
                               )
'''

# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =16
                                    , shuffle    =True
                                    #, num_workers=4
                                    , pin_memory =True
                                    )

# Test the dataset
# Fetch a sample
if False:
    dataset.__getitem__(0)

# Benchmarking
if False:
    for sample in tqdm(loader):
        test = sample

# Display some transformed images
if False:
    for sample in loader:
        FT.to_pil_image(sample["images"][0]).show()
        a=0

#%% Model
net = pnet.PNASNet5Large( n_class    =len(dataset.classes)
                        , input_shape=[1,128,128]
                        )

# Create the loss
#loss_fn = nn.CrossEntropyLoss()
loss_fn = CrossEntropyHardNegative(ratio=0.75)

# Create the optimizer
optimizer = RangerLars(net.parameters())

net.cuda()

# Example input
# Storage DataFrame
train_df = pd.DataFrame()
test_df  = pd.DataFrame()

for epoch in range(1,101):
    # Storage
    train_predictions = []
    train_truths      = []
    train_loss        = []

    test_predictions = []
    test_truths      = []
    test_loss        = []

    # Training
    dataset.train()
    net.train()
    for i,sample in enumerate(loader):
        optimizer.zero_grad()

        # Forward pass
        out = net(sample["images"].cuda())

        # Predictions
        predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

        # Loss
        loss_ = loss_fn(out.cpu(), sample["classes"])
        loss_.backward()
        optimizer.step()

        # Metrics
        train_predictions.append(predictions)
        train_truths.append(sample["classes"].numpy())
        train_loss.append(loss_.detach().item())

        # Print a summary every 100 minibatches
        if (i + 1) % 100 == 0:
            predictions = np.concatenate(train_predictions[-100:])
            truths      = np.concatenate(train_truths[-100:])
            loss        = np.array(train_loss[-100:]).mean()

            accuracy = np.equal(predictions, truths).astype(np.double).mean()

            print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

    # Testing
    dataset.eval()
    net.eval()
    with torch.no_grad():
        for i, sample in enumerate(loader):
            # Forward pass
            out = net(sample["images"].cuda())

            # Predictions
            predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

            # Loss
            loss_ = loss_fn(out.cpu(), sample["classes"])

            # Metrics
            test_predictions.append(predictions)
            test_truths.append(sample["classes"].numpy())
            test_loss.append(loss_.detach().item())

    # Print a summary of evaluation
    predictions = np.concatenate(test_predictions)
    truths      = np.concatenate(test_truths)
    loss        = np.array(test_loss).mean()

    accuracy = np.equal(predictions, truths).astype(np.double).mean()

    print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

    # Create temporary DataFrames
    # Training
    temp_train_df = pd.DataFrame({ "Epoch"     :epoch
                                 , "Prediction":np.concatenate(train_predictions)
                                 , "Target"    :np.concatenate(train_truths)
                                 }
                                )

    # Testing
    temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                , "Prediction": np.concatenate(test_predictions)
                                , "Target"    : np.concatenate(test_truths)
                                }
                               )

    # Update the full tables
    train_df = pd.concat([train_df, temp_train_df])
    test_df  = pd.concat([test_df, temp_test_df])

    # Save a distinct checkpoint every 5 epochs
    torch.save( { "epoch"               : epoch
                , "model_state_dict"    : model.state_dict()
                , "optimizer_state_dict": optimizer.state_dict()
                , "train_df"            : train_df
                , "test_df"             : test_df
                }
              , "checkpoints\\PNASNet V5 weights - checkpoint {}.tar".format(epoch)
              )

    # Summarize each epoch
    train_df_summary = temp_train_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    test_df_summary = temp_test_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    # Print a full summary every epoch
    print("Epoch {} training summary:\n".format(epoch))
    print(train_df_summary)

    print("Epoch {} testing summary:\n".format(epoch))
    print(test_df_summary)