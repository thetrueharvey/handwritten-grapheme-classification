"""
Implementation using Fast.AI as a basis
Based on the notebook available here: https://www.kaggle.com/iafoss/grapheme-fast-ai-starter-0-960-lb
"""
#%% Setup
import pandas as pd
import fastai

from fastai.vision    import *
from fastai.callbacks import SaveModelCallback

import matplotlib.pyplot as plt

#%% Parameters
label_file = "../dataset/bengaliai-cv19/train.csv"
size       = 128
batch_size = 32
nfolds     = 4
fold       = 0

#%% Dataset
# Load the labels
df = pd.read_csv(label_file)

# Create a dataset
data = ImageList.from_df( df          =df
                        , path        ='..'
                        , folder      ="dataset\\images\\"
                        , suffix      =".png"
                        , cols        ="image_id"
                        , convert_mode="L"
                        ).split_by_idx(range(fold * len(df) // nfolds, (fold + 1) * len(df) // nfolds))\
                         .label_from_df(cols=['grapheme_root','vowel_diacritic','consonant_diacritic'])\
                         .transform(get_transforms(do_flip=False, max_warp=0.1), size=size, padding_mode="zeros")\
                         .databunch(bs=batch_size).normalize(([0.0692], [0.2051]))

# Test the dataset
data.show_batch()
plt.show()
a=0
#%%