"""
Script for debugging numerous model components
"""
#%% Setup

import torch

import transform as ts
import Dataset   as ds
from models import VGG
import Loss

torch.cuda.is_available()

#%% Dataset
#%% Dataset
# Transforms
# Train transform
train_transform = ts.Transform(transforms=[ #ts.Expand(filler=(1, 1, 1), max_scale=2, chance=0.5)
                                          #, ts.Flip(chance=0.5)
                                          ts.Resize(dims=(150,150))
                                          #, ts.PhotometricDistort(chance=0.5)
                                          #, ts.Grayscale()
                                          ]
                              , norm=False
                              )

# Test transform
test_transform = ts.Transform(transforms=[ts.Resize(dims=(150,150)), ts.Grayscale()])

# Create the dataset
dataset = ds.GraphemeDataset( image_folder="dataset\\images"
                            , label_file  ="../dataset/bengaliai-cv19/train.csv"
                            , train_transforms =None
                            , test_transforms  =None
                            )

# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =48
                                    , shuffle    =True
                                    #, num_workers=4
                                    , pin_memory =True
                                    )

# Test the dataset
# Fetch a sample
if False:
    dataset.__getitem__(0)

# Benchmarking
if False:
    for sample in tqdm(loader):
        test = sample

# Display some transformed images
if False:
    for sample in loader:
        FT.to_pil_image(sample["images"][0]).show()
        a=0

#%% Model
vgg_model = VGG.VGG16(input_shape =[1, 137, 236]
                      , output_shape=dataset.feature_sizes
                      )

vgg_model.cuda()


# Create the loss
loss_fn = Loss.MultiFeatureLoss(n_features=len(dataset.feature_sizes))

# Create the optimizer
optimizer = torch.optim.Adam( params =[ {"params": vgg_model.parameters(),  "lr": 1e-4}]
                            , lr           =1e-4
                            )

# Example input
for epoch in range(100):
    for sample in loader:
        optimizer.zero_grad()
        # Forward pass
        out = vgg_model(sample["images"].cuda())

        # Loss
        loss_ = loss_fn(out, sample["labels"])
        loss_.backward()
        optimizer.step()
        print(loss_.item())


