"""
Script for testing models for root classification
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

import classification_transforms as T
from deprecated import balanced_dataset as bds

from torchvision      import transforms
from torchtools.optim import RangerLars
from models import MobileNetV2
from Loss   import CrossEntropyHardNegative

torch.cuda.is_available()

#%% Constants


#%% Dataset
# Train transform
train_transform = transforms.Compose([ T.RandomOrder([ T.RandomNoise(decay=5e-6)
                                                     , T.RandomErasures(h=[0.05,0.2], w=[0.05,0.2], decay=5e-6)
                                                     , T.RandomAffine(decay=5e-6)
                                                     , T.RandomCrop(decay=5e-6)
                                                     , T.RandomColourJitter(decay=5e-6)
                                                     , T.Skew()
                                                     , T.RandomDistortion(probability=1)
                                                     ]
                                                    )
                                     , transforms.Grayscale(num_output_channels=1)
                                     , transforms.Resize(size=(128,128))
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ transforms.Grayscale(num_output_channels=1)
                                    , transforms.Resize(size=(128, 128))
                                    , transforms.ToTensor()
                                    ]
                                   )

# Create the dataset
dataset = bds.GraphemeBalancedDataset( image_folder="dataset\\images"
                                     , label_file  ="dataset\\bengaliai-cv19\\train.csv"
                                     , feature     ="grapheme_root"
                                     , train_transforms =train_transform
                                     , test_transforms  =test_transform
                                     )
'''
dataset = ds.GraphemeDataSubset( image_folder="dataset\\images"
                               , label_file  ="../dataset/bengaliai-cv19/train.csv"
                               , feature     ="grapheme_root"
                               , classes     =[72,64,13,107,23,96,113,147,133,115,53,43,103,79,81,38,159,22,71]
                                             #[29,56,149,59,89,122,139,150,124,86,123,42,76,141,148,55,18,167,153,91,119,117,74,83,48],
                                             #[65,85,58,151,120,165,118,25,75,32,92,15,101,142,44,136,132,36,129,94,70,77,128,21,16,52,138,155,127,109,112,140,62,125,14,156,111]
                                             #[66,31,17,88,28,40,69,106,50,154,95,135,57,68,98,90,134,93,39,144,143,152,54,46,61,137,97,160]
                                             #[35,67,162,121,84,99,80,47,100,9,131,30,116,60,110,2,24,20,4,51,157,49,3,145,161,6,166,27,41,146,78,82]
                                             #[37,19,34,5,26,87,163,164,104,126,108,8,7,10,105,11,114,63,0,12,1,45,130,158,102,33,73]
                               , train_transforms =train_transform
                               , test_transforms  =test_transform
                               , include_negative =True
                               )
'''
# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =100
                                    , shuffle    =True
                                    #, num_workers=4
                                    )

#%% Model
net = MobileNetV2.MobileNetV2( n_class    =len(dataset.classes)
                             , input_shape=[1,128,128]
                             , name       ="MobileNetV2 Grapheme Root"
                             )

# Create the loss
#loss_fn = nn.CrossEntropyLoss()
loss_fn = CrossEntropyHardNegative(ratio=0.75)

# Create the optimizer
optimizer = RangerLars(net.parameters())

# Load state dicts
state_dicts = torch.load("checkpoints\\MobileNetV2 Grapheme Root Weights.tar")
model_state_dict     = state_dicts["model_state_dict"]
optimizer_state_dict = state_dicts["optimizer_state_dict"]

# Warm starting
#del model_state_dict["layers.19.weight"]
#del model_state_dict["layers.19.bias"]

# Resume from checkpoint
net.load_state_dict(model_state_dict, strict=False)
optimizer.load_state_dict(optimizer_state_dict)

net.cuda()
for state in optimizer.state.values():
    for k, v in state.items():
        if isinstance(v, torch.Tensor):
            state[k] = v.cuda()

# Example input
# Storage DataFrame
train_df = pd.DataFrame()
test_df  = pd.DataFrame()

for epoch in range(1, 101):
    # Storage
    train_predictions = []
    train_truths      = []
    train_loss        = []

    test_predictions = []
    test_truths      = []
    test_loss        = []

    # Training
    dataset.train()
    net.train()
    for i,sample in enumerate(loader):
        optimizer.zero_grad()

        # Forward pass
        out = net(sample["images"].cuda())

        # Predictions
        predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

        # Loss
        loss_ = loss_fn(out.cpu(), sample["classes"])

        # Free memory
        del (out)
        torch.cuda.empty_cache()

        # Back propogation
        loss_.backward()
        optimizer.step()

        # Metrics
        train_predictions.append(predictions)
        train_truths.append(sample["classes"].numpy())
        train_loss.append(loss_.detach().item())

        # Print a summary every 100 minibatches
        if (i + 1) % 100 == 0:
            predictions = np.concatenate(train_predictions[-100:])
            truths      = np.concatenate(train_truths[-100:])
            loss        = np.array(train_loss[-100:]).mean()

            accuracy = np.equal(predictions, truths).astype(np.double).mean()

            print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

    # Cleanup
    del(loss_)
    del(predictions)
    del(truths)
    del(loss)
    torch.cuda.empty_cache()

    # Testing
    dataset.eval()
    net.eval()
    with torch.no_grad():
        for i, sample in enumerate(loader):
            # Forward pass
            out = net(sample["images"].cuda())

            # Predictions
            predictions = torch.argmax(F.softmax(out.cpu()), dim=1).numpy()

            # Loss
            loss_ = loss_fn(out.cpu(), sample["classes"])

            # Free memory
            del (out)
            torch.cuda.empty_cache()

            # Metrics
            test_predictions.append(predictions)
            test_truths.append(sample["classes"].numpy())
            test_loss.append(loss_.detach().item())

    # Print a summary of evaluation
    predictions = np.concatenate(test_predictions)
    truths      = np.concatenate(test_truths)
    loss        = np.array(test_loss).mean()

    accuracy = np.equal(predictions, truths).astype(np.double).mean()

    print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

    # Cleanup
    del (loss_)
    del (predictions)
    del (truths)
    del (loss)
    torch.cuda.empty_cache()

    # Create temporary DataFrames
    # Training
    temp_train_df = pd.DataFrame({ "Epoch"     :epoch
                                 , "Prediction":np.concatenate(train_predictions)
                                 , "Target"    :np.concatenate(train_truths)
                                 }
                                )

    # Testing
    temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                , "Prediction": np.concatenate(test_predictions)
                                , "Target"    : np.concatenate(test_truths)
                                }
                               )

    # Update the full tables
    train_df = pd.concat([train_df, temp_train_df])
    test_df  = pd.concat([test_df, temp_test_df])

    # Save a distinct checkpoint every 5 epochs
    torch.save( { "epoch"               : epoch
                , "model_state_dict"    : net.state_dict()
                , "optimizer_state_dict": optimizer.state_dict()
                , "train_df"            : train_df
                , "test_df"             : test_df
                }
              , "checkpoints\\{} Weights.tar".format(net.name)
              )

    # Summarize each epoch
    train_df_summary = temp_train_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    test_df_summary = temp_test_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

    # Print a full summary every epoch
    print("Epoch {} training summary:\n".format(epoch))
    print(train_df_summary)

    print("Epoch {} testing summary:\n".format(epoch))
    print(test_df_summary)

