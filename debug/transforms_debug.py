"""
Script for testing custom transforms as well as the Augmentor package
"""
#%% Setup
import torch

from torchvision import transforms

from deprecated import balanced_dataset as bds
import classification_transforms as T

#%% Transforms
train_transform = transforms.Compose([ T.RandomOrder([ T.RandomNoise(decay=5e-7)
                                                     , T.RandomErasures(h=[0.05,0.2], w=[0.05,0.2], decay=5e-7)
                                                     , T.RandomAffine(decay=5e-7)
                                                     , T.RandomCrop(decay=5e-7)
                                                     , T.RandomColourJitter(decay=5e-7)
                                                     , T.Skew()
                                                     , T.RandomDistortion()
                                                     ]
                                                    )
                                     , transforms.Grayscale(num_output_channels=1)
                                     , transforms.Resize(size=(128,128))
                                     , transforms.ToTensor()
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ transforms.Grayscale(num_output_channels=1)
                                    , transforms.Resize(size=(128, 128))
                                    , transforms.ToTensor()
                                    ]
                                   )

#%% Dataset
dataset = bds.GraphemeBalancedDataset( image_folder="dataset\\images"
                                     , label_file  ="dataset\\bengaliai-cv19\\train.csv"
                                     , feature     ="consonant_diacritic"
                                     , train_transforms =train_transform
                                     , test_transforms  =test_transform
                                     )

# Create a loader
loader = torch.utils.data.DataLoader( dataset    =dataset
                                    , batch_size =128
                                    , shuffle    =True
                                    , num_workers=4
                                    , pin_memory =True
                                    )

#%% Benchmarking
for i,sample in enumerate(loader):
    print(i)


#%% Fetch a single example
for i in range(10):
    test = dataset.__getitem__(i)
    transforms.functional.to_pil_image(test["images"]).show()

a=0

#%% Augmentor




