"""
Model network
"""
#%% Setup
from tensorflow.keras.layers import Flatten, Dense, LeakyReLU, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.applications import MobileNetV2

#%% Network
def MobileNet(input_shape, n_classes):
    # Create the body of the network
    base = MobileNetV2( input_shape=input_shape
                      , include_top=False
                      , weights    =None
                      )

    # Create the dense layers
    x = Flatten()(base.output)
    x = Dropout(0.2)(x)
    x = Dense(512)(x)
    x = LeakyReLU(alpha=0.1)(x)
    x = Dense(n_classes, activation="softmax")(x)
    
    # Create the model
    model = Model( inputs =base.input
                 , outputs=x
                 )

    return model
