"""
Main script for training a TensorFlow model
"""
#%% Setup
from glob import glob

import numpy  as np
import pandas as pd

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks           import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint, TensorBoard
from tensorflow.keras.optimizers          import RMSprop, Nadam, Adadelta
from tensorflow.keras.models              import load_model

import Dataset
import Net

#%% Datasets
feature = "vowel_diacritic"


# Transforms
transform = ImageDataGenerator( rotation_range    =12
                              , zoom_range        =0.35
                              , width_shift_range =0.3
                              , height_shift_range=0.3
                              )

# Load and shuffle the dataset
df = pd.read_csv("images/train.csv")
df = df.sample(frac=1.0, random_state=0)

# Split
train_df = df.iloc[:int(len(df) * 0.7)]
test_df  = df.iloc[int(len(df) * 0.7):]

# Create the image lists
train_images = ["images/" + row["image_id"] + ".png" for i,row in train_df.iterrows()]
test_images  = ["images/" + row["image_id"] + ".png" for i,row in test_df.iterrows()]

# Create the datasets
train_ds = Dataset.Dataset( image_list=train_images
                          , label_list=train_df[feature].values
                          , batch_size=256
                          , shuffle   =True
                          , transform =transform
                          , image_params={"height":92, "width":116, "mean":[33.89], "std":[70.87]}
                          )

test_ds = Dataset.Dataset( image_list=test_images
                         , label_list=test_df[feature].values
                         , batch_size=256
                         , shuffle   =False
                         , image_params={"height":92, "width":116, "mean":[33.89], "std":[70.87]}
                         )

#%% Network components
# Network
net = Net.MobileNet(input_shape=[92,116,1], n_classes=11)

# Optimizer
optimizer = RMSprop(learning_rate=0.0002, rho=0.9, decay=1e-5)
#optimizer = Nadam()
#optimizer = Adadelta()

# Learning rate callback
learning_rate_reduction = ReduceLROnPlateau( monitor  ='loss'
                                           , factor   =0.2
                                           , patience =2
                                           , verbose  =1
                                           , mode     ="min"
                                           , min_delta=0.0001
                                           , cooldown =0
                                           , min_lr   =0.00001
                                           )

# Early stopping
early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=300, restore_best_weights=True)

# Tensorboard
tensorboard = TensorBoard(log_dir="tensorboard")

# Saving
checkpoint = ModelCheckpoint(filepath="checkpoints/MobileNetV2 Vowel.tf", monitor="val_loss", mode="min", save_best_only=True)

# Compile the network
net.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

#%% Training
#net = load_model("checkpoints/test.h5")
train_history = net.fit( generator      =train_ds
                       , validation_data=test_ds
                       , epochs         =100
                       , callbacks      =[learning_rate_reduction, early_stopping, checkpoint, tensorboard]
                       )

# Save
net.save("checkpoints/CustomNetV2.h5")
a=0