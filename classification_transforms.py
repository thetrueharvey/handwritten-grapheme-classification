"""
Transforms for classification problems (i.e. image-only)
These are transforms that are more complex than the standard TorchVision transforms
"""
#%% Setup
import random
import PIL
import torch
import Augmentor

from torchvision import transforms

#%% TODO
# Add perspective transforms (need to check how implementation works)

#%% Transforms
# Random Noise
class RandomNoise:
    def __init__( self
                , frequency  : list  = [0.05,0.4]
                , decay      : float = 1e-7
                , decay_stop : float = 0.25
                , probability: float = 0.3
                ):
        """
        Adds noise (random values) to the pixels of the image
        :param frequency: Baseline range of probability for noise
        :param decay    : The rate at which the initial frequency range decays, per image
        """
        # Class attributes
        self.frequency   = frequency
        self.decay       = decay
        self.decay_stop  = decay_stop
        self.probability = probability

        # Call counter
        self._call_counter = 0

    def __call__(self, img: torch.Tensor):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Calculate the new frequency
        frequency = random.uniform(self.frequency[0], self.frequency[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Generate random noise
        noise = torch.where(torch.rand_like(img) < frequency, (torch.rand_like(img) * 2) - 1, torch.zeros_like(img))

        # Apply the noise and clamp the values
        img = (img + noise).clamp(0,1)

        # Increment the call counter
        self._call_counter += 1

        return transforms.functional.to_pil_image(img)

class RandomErasures:
    def __init__( self
                , n_erasures : list  = [2,5]
                , h          : list  = [0.02, 0.1]
                , w          : list  = [0.02, 0.1]
                , fill       : list  = [0,1]
                , decay      : float = 1e-7
                , decay_stop : float = 0.25
                , probability: float = 0.3
                ):
        """
        Extension of the normal TorchVision RandomErase transformation, with the capability to implement multiple erasures, and anneal the transformation
        TODO: Documentation
        :param n_erasures:
        :param h:
        :param w:
        :param fill:
        :param n_decay:
        :param size_decay:
        """
        # Class attributes
        self.n_erasures  = n_erasures
        self.h           = h
        self.w           = w
        self.fill        = fill
        self.decay       = decay
        self.decay_stop  = decay_stop
        self.probability = probability

        # Call counter
        self._call_counter = 0

    def __call__(self, img: torch.Tensor):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Calculate the new number of erasures
        n_erasures = int(random.uniform(self.n_erasures[0], self.n_erasures[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop))

        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Add the erasures
        for _ in range(n_erasures):
            # Calculate the new sizes
            h = random.uniform(self.h[0], self.h[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            w = random.uniform(self.w[0], self.w[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            # Calculate the height and width
            h_ = int(h * img.shape[1])
            w_ = int(w * img.shape[2])

            # Calculate the top corner of the erasure
            x = random.randint(0, img.shape[2] - w_)
            y = random.randint(0, img.shape[1] - h_)

            # Calculate a random fill
            fill = random.uniform(self.fill[0], self.fill[1])

            # Fill the section
            img[:, y:(y + h_), x:(x + w_)] = fill

        # Increment the call counter
        self._call_counter += 1

        return transforms.functional.to_pil_image(img)

class RandomAffine:
    def __init__( self
                , n             : int   = 2
                , angle         : list  = [-15, 15]
                , translate     : tuple = ([-0.15, 0.15], [-0.15, 0.15])
                , scale         : list  = [-0.2, 0.2]
                , shear         : tuple = ([-10, 10], [-10, 10])
                , decay         : float = 1e-7
                , decay_stop    : float = 0.25
                , probabilities : list  = [0.2,0.2,0.2,0.2,0.2,0.2]
                ):
        """
        Extension of the normal TorchVision RandomAffine transformation, with the capability to anneal the transformation parameters
        TODO: Documentation
        :param n:
        :param angle:
        :param translate:
        :param scale:
        :param shear:
        :param decay:
        """
        # Class attributes
        self.n             = n
        self.angle         = angle
        self.translate     = translate
        self.scale         = scale
        self.shear         = shear
        self.decay         = decay
        self.decay_stop    = decay_stop
        self.probabilities = probabilities

        # Call counter
        self._call_counter = 0

    def __call__(self, img: torch.Tensor):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        for _ in range(self.n):
            # Calculate each parameter and check if it should be applied
            angle = 0
            if random.randrange(0, 1) < self.probabilities[0]:
                angle = random.uniform(self.angle[0], self.angle[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            translate_x = 0
            if random.randrange(0, 1) < self.probabilities[1]:
                translate_x = random.uniform(self.translate[0][0], self.translate[0][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            translate_y = 0
            if random.randrange(0, 1) < self.probabilities[2]:
                translate_y = random.uniform(self.translate[1][0], self.translate[1][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            scale = 1
            if random.randrange(0, 1) < self.probabilities[3]:
                scale = 1 + random.uniform(self.scale[0], self.scale[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            shear_x = 0
            if random.randrange(0, 1) < self.probabilities[4]:
                shear_x = random.uniform(self.shear[0][0], self.shear[0][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            shear_y = 0
            if random.randrange(0, 1) < self.probabilities[5]:
                shear_y = random.uniform(self.shear[1][0], self.shear[1][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

            # Apply the affine transformation
            img = transforms.functional.affine( img      =img
                                              , angle    =angle
                                              , translate=[translate_x, translate_y]
                                              , scale    =scale
                                              , shear    =[shear_x, shear_y]
                                              , fillcolor=random.randrange(0, 1)
                                              )

        # Increment the call counter
        self._call_counter += 1

        return img

class RandomCrop:
    def __init__( self
                , crop_range : list  = ([0.05, 0.15], [0.05, 0.15])
                , decay      : float = 1e-7
                , decay_stop : float = 0.25
                , probability: float = 0.5
                ):

        # Class attributes
        self.crop_range  = crop_range
        self.decay       = decay
        self.decay_stop  = decay_stop
        self.probability = probability

        # Call counter
        self._call_counter = 0

    def __call__(self, img):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Calculate the crop proportions
        crop_x = 1 - random.uniform(self.crop_range[0][0], self.crop_range[0][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
        crop_y = 1 - random.uniform(self.crop_range[1][0], self.crop_range[1][1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)

        # Hence calculate the absolute dimensions
        w = int(crop_x * img.width)
        h = int(crop_y * img.height)

        # Calculate the top corner of the crop
        x = random.randint(0, img.width - w)
        y = random.randint(0, img.height - h)

        # Apply the transformation
        img = transforms.functional.resized_crop( img
                                                , y
                                                , x
                                                , h
                                                , w
                                                , [ img.height, img.width]
                                                )

        # Increment the call counter
        self._call_counter += 1

        return img

class RandomColourJitter:
    def __init__( self
                , brightness   : list  = [-0.4, 0.4]
                , contrast     : list  = [-0.4, 0.4]
                , gamma        : list  = [-0.4, 0.4]
                , hue          : list  = [-0.4, 0.4]
                , saturation   : list  = [-1, 2]
                , decay        : float = 1e-7
                , decay_stop   : float = 0.25
                , probabilities: list  = [0.2,0.2,0.2,0.2,0.2]
                ):

        # Class attributes
        self.brightness    = brightness
        self.contrast      = contrast
        self.gamma         = gamma
        self.hue           = hue
        self.saturation    = saturation
        self.decay         = decay
        self.decay_stop    = decay_stop
        self.probabilities = probabilities

        # Call counter
        self._call_counter = 0

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Randomize and apply each transformation if necessary
        if random.randrange(0, 1) < self.probabilities[0]:
            brightness = 1 + random.uniform(self.brightness[0], self.brightness[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            img = transforms.functional.adjust_brightness( img=img
                                                         , brightness_factor=brightness
                                                         )

        if random.randrange(0, 1) < self.probabilities[1]:
            contrast = 1 + random.uniform(self.contrast[0], self.contrast[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            img = transforms.functional.adjust_contrast( img            =img
                                                       , contrast_factor=contrast
                                                       )

        if random.randrange(0, 1) < self.probabilities[2]:
            gamma = 1 + random.uniform(self.gamma[0], self.gamma[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            img = transforms.functional.adjust_gamma( img  =img
                                                    , gamma=gamma
                                                    )

        if random.randrange(0, 1) < self.probabilities[3]:
            hue = random.uniform(self.hue[0], self.hue[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            img = transforms.functional.adjust_hue( img       =img
                                                  , hue_factor=hue
                                                  )

        if random.randrange(0, 1) < self.probabilities[4]:
            saturation = 1 + random.uniform(self.saturation[0], self.saturation[1]) * relu(1 - self._call_counter * self.decay, stop=self.decay_stop)
            img = transforms.functional.adjust_saturation( img              =img
                                                         , saturation_factor=saturation
                                                         )

        # Increment the call counter
        self._call_counter += 1

        return img

class Skew:
    def __init__( self
                , probability=0.5
                ):
        # Create the skew transform
        p = Augmentor.Pipeline()
        p.skew_tilt(probability=probability)

        # Convert to a Torchvision transform
        self.skew = p.torch_transform()

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.skew(img)

        return img


class RandomDistortion:
    def __init__( self
                , grid       =[5,5]
                , magnitude  =8
                , probability=0.9
                ):
        # Create the skew transform
        p = Augmentor.Pipeline()
        p.random_distortion( probability=probability
                           , grid_width =grid[0]
                           , grid_height=grid[1]
                           , magnitude  =magnitude
                           )

        # Convert to a Torchvision transform
        self.random_distortion = p.torch_transform()

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.random_distortion(img)

        return img


class GaussianDistortion:
    def __init__( self
                , grid       =[6,6]
                , magnitude  =6
                , probability=0.5
                ):
        # Create the skew transform
        p = Augmentor.Pipeline()
        p.gaussian_distortion( probability=probability
                             , grid_width =grid[0]
                             , grid_height=grid[1]
                             , magnitude  =magnitude
                             , corner     ="bell"
                             , method     ="in"
                             )

        # Convert to a Torchvision transform
        self.gaussian_distortion = p.torch_transform()

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.gaussian_distortion(img)

        return img

class StepFilter:
    def __init__(self, cutoff = 0.7):
        self.cutoff = cutoff

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Apply the transformation
        img[img > self.cutoff]  = 1
        img[img <= self.cutoff] = 0

        return img



#%% Collation transforms
# Random Order
class RandomOrder:
    def __init__(self, transforms):
        """
        Applies the given transforms in a random order
        :param transforms:
        :param probability: float. The probability of any transform being used
        """
        self.transforms  = transforms

    def __call__(self, img):
        # Shuffle transforms
        random.shuffle(self.transforms)

        # Apply each transform
        for transform in self.transforms:
            img = transform(img)

        return img

class ChooseOne:
    def __init__(self, transforms):
        """
        Applies one of the provided transforms
        :param transforms:
        """
        self.transforms = transforms

    def __call__(self, img):
        # Shuffle transforms
        random.shuffle(self.transforms)

        # Apply a random transform
        img = self.transforms[0](img)

        return img



#%% Utility functions
def relu(x, stop=0.25):
    """
    Forces a result to 0 if it is negative
    :param x:
    :return:
    """
    return stop if x < stop else x