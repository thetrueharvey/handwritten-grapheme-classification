"""
This first script uses K-fold methodology to train a number of networks, to ultimately be used for ensembling
Tensorboard: tensorboard --logdir lightning_logs
"""
#%% Setup

import numpy  as np
import pandas as pd

import torch

from sklearn.model_selection import KFold

import classification_transforms as T
import Dataset                   as ds
import models.PNASNet_5_Large    as net
import LightningModel
import Loss

from torchvision      import transforms
from torchtools.optim import RangerLars
from pytorch_lightning import Trainer, Callback
from pytorch_lightning.loggers import TensorBoardLogger

from pytorch_lightning import __version__

torch.cuda.is_available()

#%% Dataset
# Train transform
# TODO: add a transform that acts as a step filter
train_transform = transforms.Compose([ T.StepFilter(cutoff=0.2)
                                     , T.ChooseOne([ T.RandomDistortion(probability=1.0, grid=[4,4], magnitude=7)
                                                   , T.RandomErasures(n_erasures=[1,2], h=[0.2, 0.4], w=[0.2, 0.4], decay=1e-5, decay_stop=1.0, probability=1.0)
                                                   , T.RandomAffine(angle=[-7,7], decay=1e-5, decay_stop=1.0, probabilities=[0.8,0.8,0.8,0.8,0.8,0.8])
                                                   , T.RandomNoise(decay=1e-5, decay_stop=1.0, probability=1.0)
                                                   , T.RandomCrop(decay=1e-7, decay_stop=0.3, probability=1.0)
                                                   #, T.RandomColourJitter(decay=1/600e3, decay_stop=0.1, probabilities=[0.2,0.2,0.2,0.2,0.2])
                                                   , T.Skew(probability=1.0)
                                                   ]
                                                  )
                                     , transforms.Grayscale(num_output_channels=1)
                                     , transforms.Resize((100, 100))
                                     , transforms.ToTensor()
                                     #, transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                                     ]
                                    )

# Test transform
test_transform = transforms.Compose([ transforms.Grayscale(num_output_channels=1)
                                    , transforms.Resize((100, 100))
                                    , transforms.ToTensor()
                                    #, transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                                    ]
                                   )

# Load the data
df = pd.read_csv("dataset/bengaliai-cv19/train.csv")

# Determine the label dimensions
label_dims = [len(df[feature].unique()) for feature in ("grapheme_root", "vowel_diacritic", "consonant_diacritic")]

# Split the main dataset into folds
folds = KFold(n_splits=5, random_state=101).split(df)

# Create the datasets
datasets = [[ds.GraphemeDataset( image_folder="dataset/images"
                               , label_df     =df.iloc[index]
                               , transforms  =train_transform if i == 0 else test_transform
                               )
            for i,index in enumerate(indices)] for indices in folds
           ]

# Create loaders
train_loaders = [[torch.utils.data.DataLoader(dataset=ds, batch_size=16, shuffle=True if i == 0 else False)
                  for i,ds in enumerate(train_val)
                 ]
                 for train_val in datasets
                ]

# Check a loader
for i,(train_loader, val_loader) in enumerate(train_loaders):
    for x in train_loader:
        break

#%% LR scheduler
class ReduceLROnPlateu(Callback):
    def __init__(self, optimizer):
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( optimizer=optimizer
                                                                   , mode="min"
                                                                   , factor=0.5
                                                                   , patience=4
                                                                   , verbose=1
                                                                   , min_lr=1e-9
                                                                   , cooldown=0
                                                                   )

    def on_epoch_end(self, trainer, pl_module):
        self.scheduler.step(pl_module.val_loss)

#%% K-fold network training
for i,(train_loader, val_loader) in enumerate(train_loaders):
    net_ = net.PNASNet5Large( n_classes=label_dims, input_shape=[1,92,116])

    # Load pretrained weights
    checkpoint = torch.load("lightning_logs/model_fold_0/version_32/checkpoints/epoch=6.ckpt")["state_dict"]
    net_.load_state_dict({key.replace("net.", ""):value for key,value in checkpoint.items()})

    # Loss
    loss = Loss.MultiheadCrossEntropyHardNegative(weights=[0.5,0.25,0.25], ratio=0.7)

    # Optimizer
    optimizer = RangerLars(params=net_.parameters())
    #optimizer = torch.optim.RMSprop(params=net.parameters(), lr=0.0001)

    # Create a Lightning model
    model = LightningModel.HandwrittenGraphemeModel( train_loader=train_loader
                                                   , val_loader  =[val_loader]
                                                   , net         =net_
                                                   , loss        =loss
                                                   , optimizer   =optimizer
                                                   )

    # Training
    trainer = Trainer( gpus             =1
                     , gradient_clip_val=0.1
                     , logger           =TensorBoardLogger( save_dir="lightning_logs"
                                                          , name    ="model_fold_{}".format(i)
                                                          )
                     , callbacks        =[ReduceLROnPlateu(optimizer)]
                     , progress_bar_refresh_rate=1
                     , fast_dev_run             =False
                     , accumulate_grad_batches  =8
                     )
    trainer.fit(model)

a=0
