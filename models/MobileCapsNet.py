"""
Capsule Network implementation with MobileNetV2 as a basis
"""
#%% Setup
import numpy as np

import torch
import torch.nn            as nn
import torch.nn.functional as F

#%% MobileNet V2 Class
class MobileCapsNet(nn.Module):
    def __init__( self
                , n_class    =1000
                , input_shape=[1,224,224]
                , width_mult =1.0
                , name       ="MobileCapsNetV2 Model"
                ):
        """
        TODO: Update documentation
        :param n_class:
        :param input_shape:
        :param width_mult:
        :param name:
        """
        super(MobileCapsNet, self).__init__()

        # Class attributes
        self.name = name

        # Network architecture          # t, c, n, s
        interverted_residual_setting = [ [1, 16, 1, 1]
                                       , [6, 24, 2, 2]
                                       , [6, 32, 3, 2]
                                       , [6, 64, 4, 2]
                                       , [6, 96, 3, 1]
                                       , [6, 160, 3, 2]
                                       , [6, 320, 1, 1]
                                       ]

        # Assertions
        assert input_shape[1] % 32 == 0

        # Create the first layer
        self.layers = nn.ModuleList([Conv2D_BN(in_features=input_shape[0], out_features=32, stride=2)])

        # Build the residual blocks
        in_channels = self.layers[0].conv.out_channels
        for t, c, n, s in interverted_residual_setting:
            out_channels = make_divisible(c * width_mult) if t > 1 else c
            for i in range(n):
                if i == 0:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =s
                                                       , expand_ratio=t
                                                       )
                                     )
                else:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =1
                                                       , expand_ratio=t
                                                       )
                                      )

                in_channels = out_channels

        # Build the remaining convolutional layer
        self.layers.append(Conv2D_BN( in_features =in_channels
                                    , out_features=make_divisible(1280 * width_mult) if width_mult > 1.0 else 1280
                                    , kernel_size =1
                                    , stride      =1
                                    , padding     =0
                                    )
                          )

        # Add the primary capsules
        self.layers.append(PrimaryCapsules( in_channels =self.layers[-1].conv.out_channels
                                          , out_channels=256
                                          , kernel_size =3
                                          , stride      =1
                                          , capsule_dim =8
                                          , num_cap_map =int(256 / 8)
                                          )
                         )

        # Forward pass to determine classification inputs
        x = torch.randn([1] + input_shape)
        for layer in self.layers:
            x = layer(x)

        num_primary_cap = x.size(1)

        # Add the classification capsules
        self.layers.append(ClassificationCapsules( num_classes   =n_class
                                                 , num_prim_cap  =num_primary_cap
                                                 , in_cap_dim    =self.layers[-1].capsule_dim
                                                 , out_cap_dim   =16
                                                 , num_iterations=3
                                                 )
                          )

        # Add the decoder
        self.decoder = Decoder( num_classes =self.layers[-1].num_classes
                              , hidden_units=[1024,2048]
                              , height      =input_shape[1]
                              , width       =input_shape[2]
                              )

        # Initialize weights
        self._init_weight()

    # Forward function
    def forward(self, x, y=None):
        # Forward pass through the model layers
        for layer in self.layers:
            x = layer(x)

        v_length = torch.norm(x, dim=-1)

        _, y_pred  = v_length.max(dim=1)
        y_pred_ohe = F.one_hot(y_pred, self.layers[-1].num_classes)

        if y is None:
            y = y_pred_ohe
        else:
            y = F.one_hot(y, self.layers[-1].num_classes)

        if self.decoder is not None:
            reconstruction = self.decoder(x, y)
        else:
            reconstruction = None

        return y_pred_ohe, reconstruction, v_length

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1.0)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


#%% Utilities
class Conv2D_BN(nn.Module):
    def __init__(self, in_features, out_features, kernel_size=3, stride=1, padding=1, groups=1, activation=True):
        """
        Convolution followed by Batch Normalization, with optional Mish activation
        :param in_features:
        :param out_features:
        :param kernel_size:
        :param stride:
        :param padding:
        :param groups:
        :param activation:
        """
        super(Conv2D_BN, self).__init__()

        # Class parameters
        self.activation = activation

        # Convolution and BatchNorm class
        self.conv = nn.Conv2d( in_channels =in_features
                             , out_channels=out_features
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =padding
                             , groups      =groups
                             , bias        =False
                             )

        self.bn = nn.BatchNorm2d(num_features=out_features)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)

        if self.activation:
            x = Mish(x)

        return x


def make_divisible(x, divisible_by=8):
    return int(np.ceil(x * 1. / divisible_by) * divisible_by)


class InvertedResidual(nn.Module):
    def __init__( self
                , in_features
                , out_features
                , stride
                , expand_ratio
                ):
        """
        MobileNet Inverted Residual block
        :param in_features:
        :param out_features:
        :param stride:
        :param expand_ratio:
        """
        super(InvertedResidual, self).__init__()

        # Input validation
        assert stride in [1, 2]

        # Class parameter
        self.stride = stride

        # Calculate the hidden dimension
        hidden_dim = int(in_features * expand_ratio)

        # Determine whether to use a residual connection
        self.res_connect = self.stride == 1 and in_features == out_features

        if expand_ratio == 1:
            conv_1 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2])

        else:
            conv_1 = Conv2D_BN(in_features  =in_features
                              , out_features=hidden_dim
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_3 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2, conv_3])

    def forward(self, x):
        if self.res_connect:
            return x + self._block_forward(x, self.layers)
        else:
            return self._block_forward(x, self.layers)

    @staticmethod
    def _block_forward(x, block):
        for layer in block:
            x = layer(x)

        return x

class PrimaryCapsules(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, capsule_dim, num_cap_map):
        super(PrimaryCapsules, self).__init__()

        # Class attributes
        self.capsule_dim = capsule_dim
        self.num_cap_map = num_cap_map

        # Capsule Convolution
        self.conv = nn.Conv2d( in_channels =in_channels
                             , out_channels=out_channels
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =0
                             )

    def forward(self, x):
        batch_size = x.size(0)
        x = self.conv(x)

        map_dim = x.size(-1)
        x = x.view(batch_size, self.capsule_dim, self.num_cap_map, map_dim, map_dim)
        x = x.view(batch_size, self.capsule_dim, -1).transpose(-1, -2)

        return squash(x)


class ClassificationCapsules(nn.Module):
    def __init__(self, num_classes, num_prim_cap, in_cap_dim, out_cap_dim, num_iterations):
        super(ClassificationCapsules, self).__init__()

        # Class attributes
        self.num_classes    = num_classes
        self.num_prim_cap   = num_prim_cap
        self.num_iterations = num_iterations

        self.W = nn.Parameter(0.01 * torch.randn(1, num_classes, num_prim_cap, out_cap_dim, in_cap_dim))

    def forward(self, x):
        batch_size = x.size(0)

        u     = x.unsqueeze(1).unsqueeze(4)
        u_hat = torch.matmul(self.W, u)
        u_hat = u_hat.squeeze(-1)

        # Detach u_hat during routing (prevents gradient flow)
        u_hat_ = u_hat.detach()

        # Routing
        b = torch.zeros(batch_size, self.num_classes, self.num_prim_cap, 1).cuda()
        for i in range(self.num_iterations - 1):
            c = F.softmax(b, dim=1)
            s = (c * u_hat_).sum(dim=2)
            v = squash(s)

            uv = torch.matmul(u_hat_, v.unsqueeze(-1))
            b += uv

        # Final routing
        c = F.softmax(b, dim=1)
        s = (c * u_hat).sum(dim=2)

        return squash(s)

class Decoder(nn.Module):
    def __init__(self, num_classes, hidden_units, height, width):
        super(Decoder, self).__init__()

        # Create the fully connected decoder layers
        self.reconstruction_layers = nn.ModuleList([ nn.Linear(in_features=16*num_classes, out_features=hidden_units[0])
                                                   , nn.Linear(in_features=hidden_units[0], out_features=hidden_units[1])
                                                   , nn.Linear(in_features=hidden_units[1], out_features=height*width)
                                                   ]
                                                  )

    def forward(self, x, y):
        x = (x * y[:, :, None].float()).view(x.size(0), -1)
        for layer, activation in zip(self.reconstruction_layers, (Mish, Mish, torch.sigmoid)):
            x = activation(layer(x))

        return x

#%% Utility functions
# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))

# Squash
def squash(x, dim=-1, epsilon=1e-7):
    # Determine the normal vector, and account for 0 values
    squared_norm = (x ** 2).sum(dim=dim, keepdim=True)
    safe_norm    = torch.sqrt(squared_norm + epsilon)

    # Determine the vector length and direction
    scale       = squared_norm / (1 + squared_norm)
    unit_vector = x / safe_norm

    return scale * unit_vector