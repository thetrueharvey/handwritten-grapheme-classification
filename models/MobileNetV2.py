"""
Implementation of MobileNet V2
Based on
"""
#%% Setup
import numpy as np

import torch
import torch.nn            as nn
import torch.nn.functional as F

#%% MobileNet V2 Class
class MobileNetV2(nn.Module):
    def __init__( self
                , n_class    =1000
                , input_shape=[1,224,224]
                , width_mult =1.0
                , name       ="MobileNetV2 Model"
                ):
        """
        TODO: Update documentation
        :param n_class:
        :param input_shape:
        :param width_mult:
        :param name:
        """
        super(MobileNetV2, self).__init__()

        # Class attributes
        self.name = name

        # Network architecture
        interverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        # Assertions
        #assert input_shape[1] % 32 == 0

        # Create the first layer
        self.layers = nn.ModuleList([Conv2D_BN(in_features=input_shape[0], out_features=32, stride=2)])

        # Build the residual blocks
        in_channels = self.layers[0].conv.out_channels
        for t, c, n, s in interverted_residual_setting:
            out_channels = make_divisible(c * width_mult) if t > 1 else c
            for i in range(n):
                if i == 0:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =s
                                                       , expand_ratio=t
                                                       )
                                     )
                else:
                    self.layers.append(InvertedResidual( in_features =in_channels
                                                       , out_features=out_channels
                                                       , stride      =1
                                                       , expand_ratio=t
                                                       )
                                      )

                in_channels = out_channels

        # Build the remaining convolutional layer
        self.layers.append(Conv2D_BN( in_features =in_channels
                                    , out_features=make_divisible(1280 * width_mult) if width_mult > 1.0 else 1280
                                    , kernel_size =1
                                    , stride      =1
                                    , padding     =0
                                    )
                          )

        # Add the classifier
        self.layers.append(nn.Linear( in_features =self.layers[-1].conv.out_channels
                                    , out_features=n_class
                                    )
                          )

        # Initialize weights
        self._init_weight()

    # Forward function
    def forward(self, x):
        # Pass through the first layer
        for layer in self.layers[:-1]:
            x = layer(x)

        # Average pooling
        x = x.mean(3).mean(2)

        # Classification
        x = self.layers[-1](x)

        return x

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            #print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1.0)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


#%% Utilities
class Conv2D_BN(nn.Module):
    def __init__(self, in_features, out_features, kernel_size=3, stride=1, padding=1, groups=1, activation=True):
        """
        Convolution followed by Batch Normalization, with optional Mish activation
        :param in_features:
        :param out_features:
        :param kernel_size:
        :param stride:
        :param padding:
        :param groups:
        :param activation:
        """
        super(Conv2D_BN, self).__init__()

        # Class parameters
        self.activation = activation

        # Convolution and BatchNorm class
        self.conv = nn.Conv2d( in_channels =in_features
                             , out_channels=out_features
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =padding
                             , groups      =groups
                             , bias        =False
                             )

        self.bn = nn.BatchNorm2d(num_features=out_features, momentum=0.9)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)

        if self.activation:
            x = F.leaky_relu(x, negative_slope=0.1)

        return x


def make_divisible(x, divisible_by=8):
    return int(np.ceil(x * 1. / divisible_by) * divisible_by)


class InvertedResidual(nn.Module):
    def __init__( self
                , in_features
                , out_features
                , stride
                , expand_ratio
                ):
        """
        MobileNet Inverted Residual block
        :param in_features:
        :param out_features:
        :param stride:
        :param expand_ratio:
        """
        super(InvertedResidual, self).__init__()

        # Input validation
        assert stride in [1, 2]

        # Class parameter
        self.stride = stride

        # Calculate the hidden dimension
        hidden_dim = int(in_features * expand_ratio)

        # Determine whether to use a residual connection
        self.res_connect = self.stride == 1 and in_features == out_features

        if expand_ratio == 1:
            conv_1 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2])

        else:
            conv_1 = Conv2D_BN(in_features  =in_features
                              , out_features=hidden_dim
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              )

            conv_2 = Conv2D_BN( in_features =hidden_dim
                              , out_features=hidden_dim
                              , kernel_size =3
                              , stride      =stride
                              , padding     =1
                              , groups      =hidden_dim
                              )

            conv_3 = Conv2D_BN( in_features =hidden_dim
                              , out_features=out_features
                              , kernel_size =1
                              , stride      =1
                              , padding     =0
                              , activation  =False
                              )

            self.layers = nn.ModuleList([conv_1, conv_2, conv_3])

    def forward(self, x):
        if self.res_connect:
            return x + self._block_forward(x, self.layers)
        else:
            return self._block_forward(x, self.layers)

    @staticmethod
    def _block_forward(x, block):
        for layer in block:
            x = layer(x)

        return x

# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))
