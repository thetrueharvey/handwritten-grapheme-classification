"""
Network class
"""
#%% Setup
import torch
import torch.nn as nn
import torch.nn.functional as F

#%% Network class
class VGG16(nn.Module):
    def __init__(self, input_shape, classes):
        """
        Builds a complete SSD model
        """
        super(VGG16, self).__init__()

        # Class attributes

        # Build the convolutions
        self._build_convolutions(input_channels=input_shape[0])

        # Pass a random input through the convolutions to get the required flattening size
        x = torch.zeros([1] + input_shape, dtype=torch.float32)
        y = self._conv_forward(x)

        # Build the predictions
        self._build_predictions( input_shape=y.shape[-1]
                               , classes    =classes
                               )

        # Initialize all weights
        self._init_weight()

    def _build_convolutions(self, input_channels):
        # Base network
        self.conv_1 = self._conv_block(in_channels=input_channels, channels=[64, 64], kernels=[3,3], paddings=[1,1])
        self.pool_1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_2 = self._conv_block( in_channels=self.conv_1[-1].out_channels
                                      , channels   =[128, 128]
                                      , kernels    =[3,3]
                                      , paddings   =[1,1]
                                      )
        self.pool_2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_3 = self._conv_block( in_channels=self.conv_2[-1].out_channels
                                      , channels   =[256, 256, 256]
                                      , kernels    =[3,3,3]
                                      , paddings   =[1,1,1]
                                      )
        self.pool_3 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)  # Ceiling here for even dims

        self.conv_4 = self._conv_block( in_channels=self.conv_3[-1].out_channels
                                      , channels   =[512, 512, 512]
                                      , kernels    =[3,3,3]
                                      , paddings   =[1,1,1]
                                      )
        self.pool_4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_5 = self._conv_block( in_channels=self.conv_4[-1].out_channels
                                      , channels   =[512, 512, 512]
                                      , kernels    =[3,3,3]
                                      , paddings   =[1,1,1]
                                      )
        self.pool_5 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)

        self.conv_6 = self._conv_block( in_channels=self.conv_5[-1].out_channels
                                      , channels   =[512, 256, 128]
                                      , kernels    =[3,3,3]
                                      , paddings   =[1,1,1]
                                      )

        # Flatten
        self.flatten = nn.Flatten()

    def _build_predictions(self, input_shape, classes):
        # Add the classification layers
        self.fc1 = nn.Linear(input_shape, 4096)
        self.fc2 = nn.Linear(self.fc1.out_features, 2048)

        self.classification_layer = nn.Linear(self.fc2.out_features, classes)

    # Convolutions forward function
    def _conv_forward(self, x):
        # Base model
        x = self.pool_1(self._block_forward(x, self.conv_1))
        x = self.pool_2(self._block_forward(x, self.conv_2))
        x = self.pool_3(self._block_forward(x, self.conv_3))
        x = self.pool_4(self._block_forward(x, self.conv_4))
        x = self.pool_5(self._block_forward(x, self.conv_5))
        x = self._block_forward(x, self.conv_6)

        # Flatten
        x = self.flatten(x)

        return x

    # Forward function
    def forward(self, x):
        # Forward through the convolutions
        x = self._conv_forward(x)

        # Forward through the fully connected layers
        x = Mish(self.fc1(x))
        x = Mish(self.fc2(x))

        classification = self.classification_layer(x)

        return classification

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1.0)
                m.bias.data.zero_()

    @staticmethod
    def _conv_block(in_channels, channels, kernels, paddings):
        """
        Defines a convolutional block (a sequential block of convolutions)
        Assumes a kernel size of 3, with padding of 1
        """
        # First convolution
        layers = nn.ModuleList([nn.Conv2d(in_channels,  channels[0], kernel_size=kernels[0], padding=paddings[0])])

        # Remaining convolutions
        for i in range(1, len(channels)):
            layers.append(nn.Conv2d(channels[i-1],  channels[i], kernel_size=kernels[i], padding=paddings[i]))
        # Check why was apparently working with incorrect kernel specifications
        return layers


    @staticmethod
    def _predictions(channels, boxes, multiplier):
        """
        Builds prediction layers
        """
        # Initialize storage
        layers = nn.ModuleList([])

        # Loop through the boxes
        for i,box in enumerate(boxes):
            layers.append(nn.Conv2d(channels[i], box * multiplier, kernel_size=3, padding=1))

        return layers

    @staticmethod
    def _block_forward(x, block):
        """
        Forward function for convolution blocks
        """
        for layer in block:
            x = Mish(layer(x))

        return x

    @staticmethod
    def _zipped_forward(inputs, block):
        """
        Passes each tensor in 'inputs' through each layer in 'blocks' in a zipped fashion
        """
        x = []
        for input, layer in zip(inputs, block):
            x.append(Mish(layer(input)))

        return x

    @staticmethod
    def _reshape(x, cols):
        """
        Reshapes a tensor according to the requirements of the SSD algorithm
        """
        x = x.permute(0, 2, 3,1).contiguous().view(x.shape[0], -1, cols)

        return x


# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))
