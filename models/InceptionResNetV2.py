"""
Implementation of InceptionResNetV2 with Mish activation
Based on: https://github.com/Cadene/pretrained-models.pytorch/blob/master/pretrainedmodels/models/inceptionresnetv2.py
"""
#%% Setup
import torch
import torch.nn as nn
import torch.nn.functional as F

#%% Model
class InceptionResNetV2(nn.Module):

    def __init__(self, n_class=1001, input_shape=[1,224,224], scaling=2):
        super(InceptionResNetV2, self).__init__()
        # Special attributs
        self.input_space = None
        self.input_shape = input_shape

        # Modules
        self.conv2d_1a  = BasicConv2d(input_shape[0], 32//scaling, kernel_size=3, stride=1)
        self.conv2d_2a  = BasicConv2d(32//scaling, 32//scaling, kernel_size=3, stride=1)
        self.conv2d_2b  = BasicConv2d(32//scaling, 64//scaling, kernel_size=3, stride=1, padding=1)
        self.maxpool_3a = nn.MaxPool2d(3, stride=2)
        self.conv2d_3b  = BasicConv2d(64//scaling, 80//scaling, kernel_size=1, stride=1)
        self.conv2d_4a  = BasicConv2d(80//scaling, 192//scaling, kernel_size=3, stride=1)
        self.maxpool_5a = nn.MaxPool2d(3, stride=2)
        self.mixed_5b   = Mixed_5b()
        self.repeat = nn.Sequential(
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17)
        )
        self.mixed_6a = Mixed_6a()
        self.repeat_1 = nn.Sequential(
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10)
        )
        self.mixed_7a = Mixed_7a()
        self.repeat_2 = nn.Sequential(
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20)
        )
        self.block8      = Block8(noReLU=True)
        self.conv2d_7b   = BasicConv2d(2080//scaling, 1536//scaling, kernel_size=1, stride=1)
        self.avgpool_1a  = nn.AvgPool2d(6, count_include_pad=False)
        self.last_linear = nn.Linear(1536//scaling, n_class)

    def features(self, input):
        x = self.conv2d_1a(input)
        x = self.conv2d_2a(x)
        x = self.conv2d_2b(x)
        x = self.maxpool_3a(x)
        x = self.conv2d_3b(x)
        x = self.conv2d_4a(x)
        x = self.maxpool_5a(x)
        x = self.mixed_5b(x)
        x = self.repeat(x)
        x = self.mixed_6a(x)
        x = self.repeat_1(x)
        x = self.mixed_7a(x)
        x = self.repeat_2(x)
        x = self.block8(x)
        x = self.conv2d_7b(x)
        return x

    def logits(self, features):
        x = self.avgpool_1a(features)
        x = x.view(x.size(0), -1)
        x = self.last_linear(x)
        return x

    def forward(self, input):
        x = self.features(input)
        x = self.logits(x)
        return x

class BasicConv2d(nn.Module):

    def __init__(self, in_planes, out_planes, kernel_size, stride, padding=0):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, bias=False) # verify bias false
        self.bn = nn.BatchNorm2d(out_planes,
                                 eps=0.001, # value found in tensorflow
                                 momentum=0.1, # default pytorch value
                                 affine=True)


    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = Mish(x)
        return x


class Mixed_5b(nn.Module):

    def __init__(self, scaling=2):
        super(Mixed_5b, self).__init__()

        self.branch0 = BasicConv2d(192//scaling, 96//scaling, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(192//scaling, 48//scaling, kernel_size=1, stride=1),
            BasicConv2d(48//scaling, 64//scaling, kernel_size=5, stride=1, padding=2)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(192//scaling, 64//scaling, kernel_size=1, stride=1),
            BasicConv2d(64//scaling, 96//scaling, kernel_size=3, stride=1, padding=1),
            BasicConv2d(96//scaling, 96//scaling, kernel_size=3, stride=1, padding=1)
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1, padding=1, count_include_pad=False),
            BasicConv2d(192//scaling, 64//scaling, kernel_size=1, stride=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)
        out = torch.cat((x0, x1, x2, x3), 1)
        return out


class Block35(nn.Module):

    def __init__(self, scale=1.0, scaling=2):
        super(Block35, self).__init__()

        self.scale = scale

        self.branch0 = BasicConv2d(320//scaling, 32//scaling, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(320//scaling, 32//scaling, kernel_size=1, stride=1),
            BasicConv2d(32//scaling, 32//scaling, kernel_size=3, stride=1, padding=1)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(320//scaling, 32//scaling, kernel_size=1, stride=1),
            BasicConv2d(32//scaling, 48//scaling, kernel_size=3, stride=1, padding=1),
            BasicConv2d(48//scaling, 64//scaling, kernel_size=3, stride=1, padding=1)
        )

        self.conv2d = nn.Conv2d(128//scaling, 320//scaling, kernel_size=1, stride=1)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        out = torch.cat((x0, x1, x2), 1)
        out = self.conv2d(out)
        out = out * self.scale + x
        out = Mish(out)
        return out


class Mixed_6a(nn.Module):

    def __init__(self, scaling=2):
        super(Mixed_6a, self).__init__()

        self.branch0 = BasicConv2d(320//scaling, 384//scaling, kernel_size=3, stride=2)

        self.branch1 = nn.Sequential(
            BasicConv2d(320//scaling, 256//scaling, kernel_size=1, stride=1),
            BasicConv2d(256//scaling, 256//scaling, kernel_size=3, stride=1, padding=1),
            BasicConv2d(256//scaling, 384//scaling, kernel_size=3, stride=2)
        )

        self.branch2 = nn.MaxPool2d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        out = torch.cat((x0, x1, x2), 1)
        return out


class Block17(nn.Module):

    def __init__(self, scale=1.0, scaling=2):
        super(Block17, self).__init__()

        self.scale = scale

        self.branch0 = BasicConv2d(1088//scaling, 192//scaling, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(1088//scaling, 128//scaling, kernel_size=1, stride=1),
            BasicConv2d(128//scaling, 160//scaling, kernel_size=(1,7), stride=1, padding=(0,3)),
            BasicConv2d(160//scaling, 192//scaling, kernel_size=(7,1), stride=1, padding=(3,0))
        )

        self.conv2d = nn.Conv2d(384//scaling, 1088//scaling, kernel_size=1, stride=1)


    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out = self.conv2d(out)
        out = out * self.scale + x
        out = Mish(out)
        return out


class Mixed_7a(nn.Module):

    def __init__(self, scaling=2):
        super(Mixed_7a, self).__init__()

        self.branch0 = nn.Sequential(
            BasicConv2d(1088//scaling, 256//scaling, kernel_size=1, stride=1),
            BasicConv2d(256//scaling, 384//scaling, kernel_size=3, stride=2)
        )

        self.branch1 = nn.Sequential(
            BasicConv2d(1088//scaling, 256//scaling, kernel_size=1, stride=1),
            BasicConv2d(256//scaling, 288//scaling, kernel_size=3, stride=2)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(1088//scaling, 256//scaling, kernel_size=1, stride=1),
            BasicConv2d(256//scaling, 288//scaling, kernel_size=3, stride=1, padding=1),
            BasicConv2d(288//scaling, 320//scaling, kernel_size=3, stride=2)
        )

        self.branch3 = nn.MaxPool2d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)
        out = torch.cat((x0, x1, x2, x3), 1)
        return out


class Block8(nn.Module):

    def __init__(self, scale=1.0, noReLU=False, scaling=2):
        super(Block8, self).__init__()

        self.scale = scale
        self.noReLU = noReLU

        self.branch0 = BasicConv2d(2080//scaling, 192//scaling, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(2080//scaling, 192//scaling, kernel_size=1, stride=1),
            BasicConv2d(192//scaling, 224//scaling, kernel_size=(1,3), stride=1, padding=(0,1)),
            BasicConv2d(224//scaling, 256//scaling, kernel_size=(3,1), stride=1, padding=(1,0))
        )

        self.conv2d = nn.Conv2d(448//scaling, 2080//scaling, kernel_size=1, stride=1)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out = self.conv2d(out)
        out = out * self.scale + x
        if not self.noReLU:
            out = Mish(out)
        return out


#%% Utility functions
# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))
