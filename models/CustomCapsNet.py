"""
Custom implementation of a Capsule Network
"""
#%% Setup
import numpy as np

import torch
import torch.nn            as nn
import torch.nn.functional as F

#%% MobileNet V2 Class
class CustomCapsNet(nn.Module):
    def __init__( self
                , n_class    =1000
                , input_shape=[1,224,224]
                , add_decoder=True
                , name       ="CustomCapsNetV2 Model"
                ):
        """
        TODO: Update documentation
        :param n_class:
        :param input_shape:
        :param width_mult:
        :param name:
        """
        super(CustomCapsNet, self).__init__()

        # Class attributes
        self.name = name

        # Convolutional cells
        self.conv_1_1 = nn.Conv2d(in_channels=input_shape[0], out_channels=16, kernel_size=3, stride=1, padding=1)
        self.conv_1_2 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, stride=1, padding=1)
        self.conv_1_3 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, stride=1, padding=1)
        self.bn_1 = nn.BatchNorm2d(num_features=16)

        self.conv_pool_1 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=2, stride=2, padding=1)

        self.conv_2_1 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv_2_2 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv_2_3 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.bn_2 = nn.BatchNorm2d(num_features=32)

        self.conv_pool_2 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=2, stride=2, padding=1)

        self.conv_3_1 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.conv_3_2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.conv_3_3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.bn_3 = nn.BatchNorm2d(num_features=64)

        self.conv_pool_3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=2, padding=1)

        self.conv_4_1 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=1, padding=1)
        self.conv_4_2 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1)
        self.conv_4_3 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1)
        self.bn_4 = nn.BatchNorm2d(num_features=128)

        self.conv_pool_4 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=2, stride=2, padding=1)

        # Add the primary capsules
        self.primary_caps = PrimaryCapsules( in_channels =self.conv_pool_4.out_channels
                                           , out_channels=256
                                           , kernel_size =3
                                           , stride      =1
                                           , capsule_dim =8
                                           , num_cap_map =int(256 / 8)
                                           )

        # Forward pass to determine classification inputs
        x = torch.randn([1] + input_shape)
        x = self._conv_forward(x)
        x = self.primary_caps(x)

        num_primary_cap = x.size(1)

        # Add the classification capsules
        self.class_caps = ClassificationCapsules( num_classes   =n_class
                                                , num_prim_cap  =num_primary_cap
                                                , in_cap_dim    =self.primary_caps.capsule_dim
                                                , out_cap_dim   =16
                                                , num_iterations=3
                                                )

        # Add the decoder
        if add_decoder:
            self.decoder = Decoder( num_classes =self.class_caps.num_classes
                                  , hidden_units=[1024,2048]
                                  , height      =input_shape[1] // 2
                                  , width       =input_shape[2] // 2
                                  )
        else:
            self.decoder = False

        # Initialize weights
        self._init_weight()

    def _conv_forward(self, x):
        # Forward pass through the convolutional layers
        x = Mish(self.conv_1_1(x))
        x = Mish(self.conv_1_2(x))
        x = Mish(self.conv_1_3(x))
        x = self.bn_1(x)
        x = Mish(self.conv_pool_1(x))

        x = Mish(self.conv_2_1(x))
        x = Mish(self.conv_2_2(x))
        x = Mish(self.conv_2_3(x))
        x = self.bn_2(x)
        x = Mish(self.conv_pool_2(x))

        x = Mish(self.conv_3_1(x))
        x = Mish(self.conv_3_2(x))
        x = Mish(self.conv_3_3(x))
        x = self.bn_3(x)
        x = Mish(self.conv_pool_3(x))

        x = Mish(self.conv_4_1(x))
        x = Mish(self.conv_4_2(x))
        x = Mish(self.conv_4_3(x))
        x = self.bn_4(x)
        x = Mish(self.conv_pool_4(x))

        return x

    # Forward function
    def forward(self, x, y=None):
        # Convolutional forward
        x = self._conv_forward(x)
        x = self.primary_caps(x)
        x = self.class_caps(x)

        v_length = torch.norm(x, dim=-1)

        _, y_pred  = v_length.max(dim=1)
        y_pred_ohe = F.one_hot(y_pred, self.class_caps.num_classes)

        if y is None:
            y = y_pred_ohe
        else:
            y = F.one_hot(y, self.class_caps.num_classes)

        if self.decoder is not None:
            reconstruction = self.decoder(x, y)
        else:
            reconstruction = None

        return y_pred_ohe, reconstruction, v_length

    # Weight initialization
    def _init_weight(self):
        for m in self.modules():
            print(m)
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1.0)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


#%% Utilities
class PrimaryCapsules(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, capsule_dim, num_cap_map):
        super(PrimaryCapsules, self).__init__()

        # Class attributes
        self.capsule_dim = capsule_dim
        self.num_cap_map = num_cap_map

        # Capsule Convolution
        self.conv = nn.Conv2d( in_channels =in_channels
                             , out_channels=out_channels
                             , kernel_size =kernel_size
                             , stride      =stride
                             , padding     =0
                             )

    def forward(self, x):
        batch_size = x.size(0)
        x = self.conv(x)

        map_dim = x.size(-1)
        x = x.view(batch_size, self.capsule_dim, self.num_cap_map, map_dim, map_dim)
        x = x.view(batch_size, self.capsule_dim, -1).transpose(-1, -2)

        return squash(x)


class ClassificationCapsules(nn.Module):
    def __init__(self, num_classes, num_prim_cap, in_cap_dim, out_cap_dim, num_iterations):
        super(ClassificationCapsules, self).__init__()

        # Class attributes
        self.num_classes    = num_classes
        self.num_prim_cap   = num_prim_cap
        self.num_iterations = num_iterations

        self.W = nn.Parameter(0.01 * torch.randn(1, num_classes, num_prim_cap, out_cap_dim, in_cap_dim))

    def forward(self, x):
        batch_size = x.size(0)

        u     = x.unsqueeze(1).unsqueeze(4)
        u_hat = torch.matmul(self.W, u)
        u_hat = u_hat.squeeze(-1)

        # Detach u_hat during routing (prevents gradient flow)
        u_hat_ = u_hat.detach()

        # Routing
        b = torch.zeros(batch_size, self.num_classes, self.num_prim_cap, 1).cuda()
        for i in range(self.num_iterations - 1):
            c = F.softmax(b, dim=1)
            s = (c * u_hat_).sum(dim=2)
            v = squash(s)

            uv = torch.matmul(u_hat_, v.unsqueeze(-1))
            b += uv

        # Final routing
        c = F.softmax(b, dim=1)
        s = (c * u_hat).sum(dim=2)

        return squash(s)

class Decoder(nn.Module):
    def __init__(self, num_classes, hidden_units, height, width):
        super(Decoder, self).__init__()

        # Class attributes
        self.height = height
        self.width  = width

        # Create the fully connected decoder layers
        self.reconstruction_layers = nn.ModuleList([ nn.Linear(in_features=32*num_classes, out_features=hidden_units[0])
                                                   , nn.Linear(in_features=hidden_units[0], out_features=hidden_units[1])
                                                   , nn.Linear(in_features=hidden_units[1], out_features=height*width)
                                                   ]
                                                  )

    def forward(self, x, y):
        x = (x * y[:, :, None].float()).view(x.size(0), -1)
        for layer, activation in zip(self.reconstruction_layers, (Mish, Mish, torch.sigmoid)):
            x = activation(layer(x))

        x = x.view(-1, self.height, self.width)

        return x

#%% Utility functions
# Mish Activation
def Mish(x):
    r"""
    Mish activation function is proposed in "Mish: A Self
    Regularized Non-Monotonic Neural Activation Function"
    paper, https://arxiv.org/abs/1908.08681.
    """

    return x * torch.tanh(F.softplus(x))

# Squash
def squash(x, dim=-1, epsilon=1e-7):
    # Determine the normal vector, and account for 0 values
    squared_norm = (x ** 2).sum(dim=dim, keepdim=True)
    safe_norm    = torch.sqrt(squared_norm + epsilon)

    # Determine the vector length and direction
    scale       = squared_norm / (1 + squared_norm)
    unit_vector = x / safe_norm

    return scale * unit_vector