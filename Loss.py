"""
Object detection loss function
"""
# setup
import torch
import torch.nn            as nn
import torch.nn.functional as F

class MultiheadCrossEntropyHardNegative(nn.Module):
    def __init__(self, weights, ratio):
        """
        Multihead CrossEntropy loss with hard-negative mining
        :param ratio: float. The ratio of results that should be considered to be 'hard negative'
        """
        super(MultiheadCrossEntropyHardNegative, self).__init__()

        # Class attributes
        self.ratio   = ratio
        self.weights = weights

        # Instantiate the loss
        self.losses = [nn.CrossEntropyLoss(reduction="none") for head in range(len(weights))]

    def forward(self, out, labels):
        # Loss for each head
        # Calculate the per-element loss
        losses = [loss_(out_, labels_) for loss_, out_, labels_ in zip(self.losses, out, labels)]

        # Aggregate to per sample
        loss = torch.sum(torch.stack([loss * weight for loss, weight in zip(losses, self.weights)]), dim=0)

        # Order the loss
        loss, _ = loss.sort(descending=True)

        # Determine the hard and easy loss
        hard_loss = loss[:(int(loss.shape[0] * self.ratio))]

        # Reduce
        loss = hard_loss.sum() / hard_loss.shape[0]

        return loss

class CrossEntropyHardNegative(nn.Module):
    def __init__(self, ratio):
        """
        CrossEntropy loss with hard-negative mining
        :param ratio: float. The ratio of results that should be considered to be 'hard negative'
        """
        super(CrossEntropyHardNegative, self).__init__()

        # Class attributes
        self.ratio = ratio

        # Instantiate the loss
        self.loss = nn.CrossEntropyLoss(reduction="none")

    def forward(self, out, labels):
        # Calculate the per-element loss
        loss = self.loss(out, labels)

        # Order the loss
        loss, _ = loss.sort(descending=True)

        # Determine the hard and easy loss
        hard_loss = loss[:(int(loss.shape[0] * self.ratio))]

        # Reduce
        loss = hard_loss.sum() / hard_loss.shape[0]

        return loss
