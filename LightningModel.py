"""
Creates a PyTorch Lightning model for easy training
"""
#%% Setup
import torch
import torch.nn.functional as F
import pytorch_lightning   as pl

#%% Model class
class HandwrittenGraphemeModel(pl.LightningModule):
    def __init__( self
                , train_loader
                , val_loader
                , net
                , loss
                , optimizer
                ):
        super(HandwrittenGraphemeModel, self).__init__()

        # Class attributes
        self.train_loader = train_loader
        self.val_loader   = val_loader
        self.net          = net
        self.loss_fn      = loss
        self.optimizer    = optimizer

        # Instantiate validation loss for the learning rate scheduler
        self.val_loss = 0

    def forward(self, x):
        return self.net(x)

    def training_step(self, batch, batch_idx):
        y_   = self.forward(batch["image"])
        loss = self.loss_fn(y_, batch["label"])

        # Calculate the prediction accuracy
        accuracy = [torch.eq( torch.argmax(F.softmax(y, dim=-1), dim=1)
                            , label
                            ).float().mean()
                    for y, label in zip(y_, batch["label"])
                   ]

        return {"loss": loss, "train_acc": accuracy, "log": { "train_loss": loss
                                                            , "1. root_acc"     : accuracy[0]
                                                            , "2. vowel_acc"    : accuracy[1]
                                                            , "3. consonant_acc": accuracy[2]
                                                            }
               }

    def validation_step(self, batch, batch_idx):
        y_ = self.forward(batch["image"])
        loss = self.loss_fn(y_, batch["label"])

        # Calculate the prediction accuracy
        accuracy = [torch.eq( torch.argmax(F.softmax(y, dim=-1), dim=1)
                            , label
                            ).float().mean()
                    for y, label in zip(y_, batch["label"])
                   ]


        return {"val_loss": loss, "log": { "val_loss"        : loss
                                         , "val_root_acc"     : accuracy[0]
                                         , "val_vowel_acc"    : accuracy[1]
                                         , "val_consonant_acc": accuracy[2]
                                         }
               }

    def validation_end(self, outputs):
        # Validation set loss
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        self.val_loss = avg_loss

        tensorboard_logs = {'val_loss': avg_loss}
        return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

    def configure_optimizers(self):
        return self.optimizer

    def train_dataloader(self):
        return self.train_loader

    def val_dataloader(self):
        return self.val_loader
