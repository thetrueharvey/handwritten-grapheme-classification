"""
Notebook for testing submission of results to Kaggle
"""
#%% Setup
from glob   import glob
from PIL    import Image
from pickle import load

import numpy  as np
import pandas as pd

from torch                  import cat, no_grad
from torch.utils.data       import DataLoader
from torchvision.transforms import ToTensor, Grayscale, Resize, Compose

#%% Preprocessing
# Image dimensions
HEIGHT = 137
WIDTH  = 236

# Bounding Box
def bbox(image):
    """
    Determines the bounding boxes for images to remove empty space where possible
    :param image:
    :return:
    """
    for i in range(image.shape[1]):
        if not np.all(image[:,i] == 0):
            x_min = i - 13 if (i > 13) else 0
            break

    for i in reversed(range(image.shape[1])):
        if not np.all(image[:,i] == 0):
            x_max = i + 13 if (i < WIDTH - 13) else WIDTH
            break

    for i in range(image.shape[0]):
        if not np.all(image[i] == 0):
            y_min = i - 10 if (i > 10) else 0
            break

    for i in reversed(range(image.shape[0])):
        if not np.all(image[i] == 0):
            y_max = i + 10 if (i < HEIGHT - 10) else HEIGHT
            break

    return x_min, y_min, x_max, y_max

# Convert the files to images
for fl in glob("dataset/bengaliai-cv19/test_image_data_*.parquet"):
    # Load the dataset
    df = pd.read_parquet(fl)

    # Fetch the images and IDs
    ids, images = df.iloc[:, 0], df.iloc[:, 1:].values.reshape(-1, HEIGHT, WIDTH)
    del(df)

    # Save each image
    for id,image in zip(ids, images):
        # Input data is inverted
        image_ = 255 - image

        # Remove the boundaries of the image
        image_ = image_[5:-5, 5:-5]

        # Apply max normalization
        image_ = (image_ * (255.0 / image_.max())).astype(np.uint8)

        # Filter low-intensity pixels
        image_[image_ < 50]  = 0
        #image_[image_ >= 100] = 255

        # Crop the image
        x_min, y_min, x_max, y_max = bbox(image_)
        image_ = image_[y_min:y_max, x_min:x_max]

        image_ = Image.fromarray(image_)

        # Save
        image_.save("dataset/submission/images/{}.png".format(id))

#%% Datasets
class Dataset(object):
    def __init__(self, image_folder:str):
        """
        Class for creating a dataset for model inference
        """
        # Class attributes
        self.image_list = glob(image_folder + "/*.png")
        self.transform  = Compose([ Grayscale(num_output_channels=1)
                                  , Resize(size=(128, 128))
                                  , ToTensor()
                                  ]
                                 )

    def __len__(self):
        return len(self.image_list)

    def __getitem__(self, idx):
        # Load the image
        img = Image.open(self.image_list[idx])

        # Convert to a tensor
        img = self.transform(img)

        return img

# Create a dataset
ds = Dataset(image_folder="dataset/submission/images")

# Create a loader
loader = DataLoader(ds, batch_size=512)

#%% Models
# Load the models
model_root = "dataset/submission/models/"
models = {}
for feature,path in zip(["root", "vowel", "consonant"], [model_root + "root.pth", model_root + "vowel.pth", model_root + "consonant.pth"]):
    with open(path, mode="rb") as f:
        models[feature] = load(f)

# Predict
predictions = {}
for feature,model in models.items():
    model.cuda()
    model.eval()
    with no_grad():
        predictions[feature] = cat([model(x.cuda()).cpu() for x in loader]).softmax(dim=-1).argmax(dim=-1).numpy()

#%% Output formatting
ids    = []
labels = []
for i in range(len(predictions["root"])):
    ids.append("Test_{}_grapheme_root".format(i))
    ids.append("Test_{}_vowel_diacritic".format(i))
    ids.append("Test_{}_consonant_diacritic".format(i))

    labels.append(predictions["root"][i])
    labels.append(predictions["vowel"][i])
    labels.append(predictions["consonant"][i])

# Create a submission DataFrame
submission_df = pd.DataFrame({"row_id":ids, "target":labels})

# Write output
submission_df.to_csv('submission.csv', index=False)
