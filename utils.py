"""
Collection of utility functions for model training and building
"""
#%% Setup
import numpy  as np
import pandas as pd

import torch
import torch.nn.functional as F

#%% Function
def train( name
         , dataset
         , loader
         , net
         , loss_fn
         , optimizer
         , scheduler
         , scheduler_type="per batch"
         , epoch_start   =1
         , epoch_end     =100
         , half          =False
         , early_stopping=10
         , start_accuracy=0
         , save_dir      ="checkpoints/"
         ):
    """

    :param name:
    :param dataset:
    :param loader:
    :param net:
    :param loss_fn:
    :param optimizer:
    :param epoch_start:
    :param epoch_end:
    :param half:
    :param early_stopping:
    :return:
    """
    train_df = pd.DataFrame()
    test_df  = pd.DataFrame()

    # Track the best accuracy and the epochs since testing accuracy improved
    best_acc = start_accuracy
    stationary_epochs = 0

    # Convert to half precision if desired
    if half:
        net.half()

    for epoch in range(epoch_start, epoch_end + 1):
        # Storage
        train_predictions = []
        train_truths      = []
        train_loss        = []

        test_predictions = []
        test_truths      = []
        test_loss        = []

        # Training
        net.train()
        dataset.train()
        for i, sample in enumerate(loader):
            optimizer.zero_grad()

            # Forward pass
            if half:
                out = net(sample["image"].half().cuda()).cpu().float()
            else:
                out = net(sample["image"].cuda()).cpu().float()

            # Predictions
            predictions = torch.argmax(F.softmax(out), dim=1).numpy()

            # Loss
            loss_ = loss_fn(out, sample["label"])

            # Back propogation
            loss_.backward()
            optimizer.step()

            # Scheduler update
            if scheduler_type == "per batch":
                scheduler.step()

            # Metrics
            train_predictions.append(predictions)
            train_truths.append(sample["label"].numpy())
            train_loss.append(loss_.detach().item())

            # Print a summary every 100 minibatches
            if (i + 1) % 10 == 0:
                predictions = np.concatenate(train_predictions[-10:])
                truths = np.concatenate(train_truths[-10:])
                loss = np.array(train_loss[-10:]).mean()

                accuracy = np.equal(predictions, truths).astype(np.double).mean()

                print("Epoch: {} Train Loss: {} Train Accuracy: {}".format(epoch, loss, accuracy))

        # Testing
        net.eval()
        dataset.eval()
        with torch.no_grad():
            for i, sample in enumerate(loader):
                # Forward pass
                if half:
                    out = net(sample["image"].half().cuda()).cpu().float()
                else:
                    out = net(sample["image"].cuda()).cpu().float()

                # Predictions
                predictions = torch.argmax(F.softmax(out), dim=1).numpy()

                # Loss
                loss_ = loss_fn(out, sample["label"])

                # Metrics
                test_predictions.append(predictions)
                test_truths.append(sample["label"].numpy())
                test_loss.append(loss_.detach().item())

        # Print a summary of evaluation
        predictions = np.concatenate(test_predictions)
        truths      = np.concatenate(test_truths)
        loss        = np.array(test_loss).mean()

        accuracy = np.equal(predictions, truths).astype(np.double).mean()

        # Update learning rate
        if scheduler_type == "per epoch":
            scheduler.step(accuracy)

        print("Epoch: {} Test Loss: {} Test Accuracy: {}".format(epoch, loss, accuracy))

        # Cleanup
        torch.cuda.empty_cache()

        # Create temporary DataFrames
        # Training
        temp_train_df = pd.DataFrame({ "Epoch"     : epoch
                                     , "Prediction": np.concatenate(train_predictions)
                                     , "Target"    : np.concatenate(train_truths)
                                     }
                                    )

        # Testing
        temp_test_df = pd.DataFrame({ "Epoch"     : epoch
                                    , "Prediction": np.concatenate(test_predictions)
                                    , "Target"    : np.concatenate(test_truths)
                                    }
                                   )

        # Update the full tables
        train_df = pd.concat([train_df, temp_train_df])
        test_df  = pd.concat([test_df, temp_test_df])

        # Save a distinct checkpoint every 5 epochs
        torch.save({ "epoch": epoch
                   , "model_state_dict"    : net.state_dict()
                   , "optimizer_state_dict": optimizer.state_dict()
                   , "train_df"            : train_df
                   , "test_df"             : test_df
                   }
                  , "{}/{} Weights.tar".format(save_dir, name)
                  )

        if accuracy > best_acc:
            best_acc = accuracy

            torch.save({ "epoch": epoch
                       , "model_state_dict"    : net.state_dict()
                       , "optimizer_state_dict": optimizer.state_dict()
                       , "train_df"            : train_df
                       , "test_df"             : test_df
                       }
                      , "{}/{} Best Weights.tar".format(save_dir, name)
                      )

            stationary_epochs = 0
        else:
            stationary_epochs += 1

        # Summarize each epoch
        train_df_summary = temp_train_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()
        test_df_summary  = temp_test_df.eval("correct = Target == Prediction").groupby("Target")["correct"].mean().reset_index()

        # Print a full summary every epoch
        print("Epoch {} training summary:\n".format(epoch))
        print(train_df_summary)

        print("Epoch {} testing summary:\n".format(epoch))
        print(test_df_summary)

        if stationary_epochs == early_stopping:
            print("Training stopped since no accuracy improvement was measured")
            return best_acc, epoch

    return best_acc, epoch
