"""
Script for performing dataset inference using the trained model(s)
"""
#%% Setup
from glob import glob

import numpy  as np
import pandas as pd

import torch

from PIL       import Image

import transform      as ts
from deprecated import model as md

#%% Constants
image_folder ="dataset\\images"
label_file   ="dataset\\bengaliai-cv19\\train.csv"

model_folder = "checkpoints\\models\\"

#%% Dataset
# Create a dataset class
class FullDataset(object):
    def __init__(self, image_folder, label_file):
        self.image_folder = image_folder
        self.labels       = pd.read_csv(label_file)
        self.transforms   = ts.Transform( transforms=[ ts.Resize(dims=(128,128))
                                                     , ts.Grayscale()
                                                     ]
                                        , mean=[0.0692]
                                        , std =[0.2051]
                                        )

    def __len__(self):
        #return len(self.labels)
        return 20000

    def __getitem__(self, idx):
        # Load the labels
        label = self.labels.iloc[idx]

        # Load the image
        img = Image.open(self.image_folder + "\\" + label["image_id"] + ".png")

        # Populate the labels
        labels = [label[feature] for feature in ["grapheme_root", "vowel_diacritic", "consonant_diacritic"]]

        # Apply transformations
        img = self.transforms(img)

        return {"images": img, "classes": labels}

# Create the dataset
ds = FullDataset(image_folder=image_folder, label_file=label_file)

# Create a loader
loader = torch.utils.data.DataLoader(dataset=ds, batch_size=512, shuffle=False, pin_memory=True)

#%% Consonant Models
if False:
    consonant_models = [md.GraphemeModel.load(model_folder + model) for model in [ "MobileNetV2 consonant_diacritic 0"
                                                                                 , "MobileNetV2 consonant_diacritic 1"
                                                                                 , "MobileNetV2 consonant_diacritic 2"
                                                                                 ]
                       ]

    # Get predictions from each model
    consonant_predictions = np.concatenate([model.predict(loader) for model in consonant_models], axis=1)

    # Get the original feature space order
    classes = []
    for model in consonant_models:
        classes = classes + model.dataset.classes

    classes_lookup = {key:i for i,key in enumerate(classes)}

    # Rebuild the predictions
    predictions = [[] for _ in classes_lookup]
    for key,position in classes_lookup.items():
        predictions[key] = np.expand_dims(consonant_predictions[:,position], 1)

    # Concatenate and get the maximum prediction
    predictions = np.argmax(np.concatenate(predictions, axis=1), axis=1)

    # Fetch the ground truths
    ground_truths = np.concatenate([sample["classes"][2].numpy() for sample in loader])

    # Compare the predictions to the ground truths
    results = np.equal(predictions, ground_truths)
    results = results.sum() / len(results)
    a=0

# %% Vowel Models
if False:
    vowel_models = [md.GraphemeModel.load(model_folder + model) for model in [ "MobileNetV2 vowel_diacritic 0"
                                                                             , "MobileNetV2 vowel_diacritic 1"
                                                                             , "MobileNetV2 vowel_diacritic 2"
                                                                             ]
                   ]

    # Get predictions from each model
    vowel_predictions = np.concatenate([model.predict(loader) for model in vowel_models], axis=1)

    # Get the original feature space order
    classes = []
    for model in vowel_models:
        classes = classes + model.dataset.classes

    classes_lookup = {key: i for i, key in enumerate(classes)}

    # Rebuild the predictions
    predictions = [[] for _ in classes_lookup]
    for key, position in classes_lookup.items():
        predictions[key] = np.expand_dims(vowel_predictions[:, position], 1)

    # Concatenate and get the maximum prediction
    predictions = np.argmax(np.concatenate(predictions, axis=1), axis=1)

    # Fetch the ground truths
    ground_truths = np.concatenate([sample["classes"][1].numpy() for sample in loader])

    # Compare the predictions to the ground truths
    results = np.equal(predictions, ground_truths)
    results = results.sum() / len(results)
    a = 0

# %% Root Models
if True:
    root_models = [md.GraphemeModel.load(model) for model in glob(model_folder + "*root*")]

    # Get predictions from each model
    root_predictions = np.concatenate([model.predict(loader) for model in root_models], axis=1)

    # Get the original feature space order
    classes = []
    for model in root_models:
        classes = classes + model.dataset.classes

    classes_lookup = {key: i for i, key in enumerate(classes)}

    # Rebuild the predictions
    predictions = [[] for _ in classes_lookup]
    for key, position in classes_lookup.items():
        predictions[key] = np.expand_dims(root_predictions[:, position], 1)

    # Concatenate and get the maximum prediction
    predictions = np.argmax(np.concatenate(predictions, axis=1), axis=1)

    # Fetch the ground truths
    ground_truths = np.concatenate([sample["classes"][0].numpy() for sample in loader])

    # Compare the predictions to the ground truths
    results = np.equal(predictions, ground_truths)
    results = results.sum() / len(results)
    a = 0
